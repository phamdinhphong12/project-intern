import React, { useEffect, useState } from 'react'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import { Avatar, Breadcrumb, Col, Row, Table, Tag, Divider, Button, Steps, Space, Tooltip, Tabs, Grid } from 'antd'
import { ShoppingCartOutlined, CloseCircleFilled, EyeTwoTone, SolutionOutlined, EditOutlined, CheckSquareOutlined, ContainerOutlined } from '@ant-design/icons';
import "../../CSS/History.css"
import { } from 'react-redux'
import { useDispatch, useSelector } from 'react-redux'
import { getAllOrders } from '../../API/apiRequest'
import { useNavigate } from 'react-router'
import Money from '../../components/Money'
import moment from 'moment';
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css/scrollbar";
import Link from 'antd/es/typography/Link'
import { getAllProduct } from '../../API/apiRequest';
import { CancelOrder } from '../../components/Order/cancelModal';
import { DetailOrder } from '../../components/Order/detailModal';
import { DetailOrderProducts } from '../../components/Order/detailOrderPro';
import "../../CSS/Tabs.css"


const { useBreakpoint } = Grid;

export default function OrderHistory() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const screens = useBreakpoint();
    const { Step } = Steps;
    const [data, setData] = useState([])
    const [dataProduct, setDataProduct] = useState([])
    const [dataHistory, setDataHistory] = useState([])
    const [orderAll, setOderAll] = useState([])
    const [activeTab, setActiveTab] = useState("1")
    const [showCancelButton, setShowCancelButton] = useState(true);
    const [openCancel, setOpenCancel] = useState(false);
    const [openDetail, setOpenDetail] = useState(false);
    const [openOrderProducts, setOpenOrderProducts] = useState(false)
    const [item, setItem] = useState()


    const [currentOrder, setCurrentOrder] = useState(3)

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser);
    const orderList = useSelector((state) => state.orders.orderList?.orderData)
    const productsData = useSelector((state) => state.product.productList?.products)
    const userId = user?.user._id
    const accessToken = user?.accessToken

    const getOrderHistory = () => {
        getAllOrders(userId, dispatch)
        getAllProduct(dispatch, 0, accessToken)
    }

    const handleCancelOrder = (value) => {
        setOpenCancel(true);
        setItem(value)
    }

    const handleDetail = (value) => {
        setOpenDetail(true);
        setItem(value)
    }

    const handleSeeProducts = (value) => {
        setOpenOrderProducts(true);
        setItem(value)
    }

    const handleTabChange = (key) => {
        setActiveTab(key)
    }

    useEffect(() => {
        if (!user) {
            navigate("/")
        }
        getOrderHistory()
    }, [])

    useEffect(() => {
        if (activeTab === "3" || activeTab === "1") {
            setShowCancelButton(false);
        } else {
            setShowCancelButton(true);
        }
    }, [activeTab]);



    useEffect(() => {
        // setData(orderList)
        setDataProduct(productsData.products)
        setOderAll(orderList)

        const filterOrder = orderList.filter((item) => {
            if (item.order_status.includes("success") || item.order_status.includes("cancel")) {
                return item
            }
        })
        setDataHistory(filterOrder)

        const orderProcess = orderList.filter((item) => {
            if (item.order_status.includes("order")) {
                return item
            }
        })
        setData(orderProcess)

    }, [orderList, productsData])


    const columns = [
        {
            title: 'STT',
            key: 'stt',
            width: 70,
            render: (text, record, index) => <span>{index + 1}</span>,
        },
        {
            title: 'Thông tin người đặt',
            dataIndex: 'customer_name',
            key: 'customer_name',
            width: 250,
            render: (_, value) => <>
                <Row style={{ alignItems: 'center', justifyContent: 'space-between', width: 220 }}>
                    <Col>
                        <span>Tên: <b>{value.customer_name}</b></span>
                        <br />
                        <span>SĐT: <b>{value.customer_phone}</b></span>
                        <br />
                        <span>Địa chỉ: <b>{value.customer_address}</b></span>
                    </Col>
                </Row>

            </>
        },
        {
            title: 'Các sản phẩm đã đặt',
            dataIndex: 'order_products',
            key: 'order_products',
            width: 250,
            render: (_, record) => (
                <Button onClick={() => { handleSeeProducts(record) }} style={{ backgroundColor: "#e83e8c", color: "white", border: "none" }}>
                    {record.order_products[0].item.length} sản phẩm
                </Button>
                // <>
                //     {value.map((orderProduct, index) => (
                //         <Col key={index}>
                //             {orderProduct.item.map((item, idx) => (
                //                 <>
                //                     <Col key={idx}>
                //                         <span>Tên sản phẩm: <b style={{ color: '#e83e8c' }}>{item.name}</b> </span>
                //                         <br />
                //                         <span>Kích cỡ: <b>{item.size?.join(', ')}</b></span>
                //                         <br />
                //                         <span>Số lượng: <b>{item.quantity}</b></span>
                //                         <br />
                //                         <span>Ghi chú sản phẩm: <b>{item.notes}</b></span>
                //                     </Col>
                //                     <br />
                //                 </>

                //             ))}
                //             <br />
                //             <b>Tổng số lượng: <b style={{ color: '#e83e8c' }}>{orderProduct.quantity}</b></b>
                //             <br />
                //             <b>Thành tiền:  <Tag color='green'><Money value={orderProduct.price} /></Tag></b>
                //         </Col>

                //     ))
                //     }
                // </>
            ),
        },
        {
            title: 'Ảnh sản phẩm',
            dataIndex: 'order_products',
            key: 'order_products',
            width: 250,
            render: (value) => (
                <Avatar.Group size="large" maxCount={3}>
                    {value.map((orderProduct) => (
                        <>
                            {orderProduct.item.map((item, idx) => (
                                <Avatar key={idx} src={item.avatar} />
                            ))}
                        </>
                    ))}
                </Avatar.Group>
            )
        },
        {
            title: 'Thời gian',
            dataIndex: 'createdAt',
            key: 'createdAt',
            width: 180,
            render: (value) => (
                <Row style={{ alignItems: 'center', justifyContent: 'space-between', width: 220 }}>
                    <Col>
                        <span>Ngày: <b>{moment(value).format('YYYY-MM-DD')}</b></span>
                        <br />
                        <span>Giờ: <b>{moment(value).format('HH:mm:ss')}</b></span>
                    </Col>
                </Row>
            )
        },
        {
            title: 'Loại thanh toán',
            dataIndex: 'order_pay',
            key: 'order_pay',
            render: (value) => (
                <>
                    <Tag color={value === "1" ? 'green' : 'blue'}>
                        {value === "1" ? "Tiền mặt" : "Banking"}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Trạng thái đơn',
            key: 'order_status',
            dataIndex: 'order_status',
            render: (value) => (
                value.map((item, idx) =>
                    <Tag key={idx} color={item === "order" || item === "cancel" ? "red" : "purple"}>
                        {item === "order" ? "ĐANG XỬ LÝ ..." : item === "cancel" ? "ĐƠN ĐÃ HỦY" : "THÀNH CÔNG"}
                    </Tag>
                )
            )
        },
        {
            title: 'Chức năng',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Tooltip placement="top" title="Chi tiết" onClick={() => handleDetail(record)}>
                        <EyeTwoTone twoToneColor="#531dab" />
                    </Tooltip>
                    {showCancelButton && <Tooltip placement="top" title="Hủy đơn hàng" >
                        <CloseCircleFilled style={{ color: "red" }} onClick={() => handleCancelOrder(record)} />
                    </Tooltip>}

                </Space>
            ),
        },
    ];
    return (
        <>
            <Header />
            <Col>
                <Col span={24} className='header_history' >
                    <h2>HISTORY PAGE</h2>
                    <Breadcrumb
                        routes={[
                            { path: '/', breadcrumbName: "Home" },
                            { path: '/categories', breadcrumbName: 'Đơn hàng' }]}
                        separator="/"
                        style={{
                            fontSize: 15,
                            fontWeight: 'bold'
                        }}
                    />
                </Col>
                {orderList?.length === 0 ? "" : <Col span={16} style={{ marginTop: 30, marginLeft: "15%", marginBottom: 50 }}>
                    <Steps current={currentOrder} >
                        <Step title="Đặt hàng" />
                        <Step title="Xác nhận đơn hàng" />
                        <Step title="Đang giao hàng" />
                        <Step title="Thành công" />
                    </Steps>
                </Col>}

                <Col className='wrapp_history' style={{ marginBottom: "7%" }}>
                    <Col >
                        <Col className='cart_table_history'>
                            <h1 style={{ textAlign: 'center' }}>✨ ĐƠN HÀNG ✨ </h1>
                            <Divider />
                            <Tabs
                                defaultActiveKey="1"
                                activeKey={activeTab}
                                onChange={handleTabChange}
                                items={[
                                    {
                                        label: <span>
                                            <ContainerOutlined />
                                            Tất cả đơn
                                            <Tag color="#e83e8c" style={{ marginLeft: 5 }}>{orderAll.length}</Tag>
                                        </span>,
                                        key: '1',
                                        children: <Table columns={columns} dataSource={orderAll} scroll={{ y: 400 }} />,
                                    },
                                    {
                                        label: <span>
                                            <SolutionOutlined />
                                            Đơn hàng của tôi
                                            <Tag color="#e83e8c" style={{ marginLeft: 5 }}>{data.length}</Tag>
                                        </span>,
                                        key: '2',
                                        children: <Table columns={columns} dataSource={data} scroll={{ y: 400 }} />,
                                    },
                                    {
                                        label:
                                            <span>
                                                <CheckSquareOutlined />
                                                Lịch sử đơn hàng
                                                <Tag color="#e83e8c" style={{ marginLeft: 5 }}>{dataHistory.length}</Tag>
                                            </span>,
                                        key: '3',
                                        children: <Table columns={columns} dataSource={dataHistory} scroll={{ y: 400 }} />,
                                    },
                                    {
                                        label: <span>
                                            <EditOutlined />
                                            Đánh giá
                                        </span>,
                                        key: '4',
                                        disabled: true,
                                    },
                                ]}
                            />
                            {/* <Col className='table' >
                                <Table columns={columns} dataSource={data} scroll={{ y: 400 }} />
                            </Col> */}
                        </Col>
                    </Col>
                </Col>
            </Col>
            <Col style={{ marginBottom: 60 }}>
                <h1 style={{ textAlign: 'center', marginTop: 50, marginBottom: 30 }}>LATEST PRODUCTS</h1>
                <Row style={{
                    justifyContent: "center",
                    textAlign: 'center',
                    alignItems: 'center',
                    overflowX: 'hidden',
                    marginBottom: 50,
                    padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0
                }}>
                    <Swiper
                        slidesPerView={4}
                        spaceBetween={10}
                        className="mySwiper"
                        autoplay={{
                            delay: 1000,
                            disableOnInteraction: false,
                        }}
                        modules={[Autoplay]}
                        breakpoints={{
                            1724: {
                                slidesPerView: 4,
                            },
                            768: {
                                slidesPerView: 3,
                            },
                            500: {
                                slidesPerView: 2,
                            },
                            300: {
                                slidesPerView: 1,
                            },
                            100: {
                                slidesPerView: 1,
                            }
                        }}
                    >
                        {dataProduct?.map((item) => {
                            return (
                                <SwiperSlide key={item.id}>
                                    <Col className='slide_img' onClick={() => handleDetail(item._id)}>
                                        <Col>
                                            <span className='text_new'>NEW</span>
                                        </Col>
                                        <img src={item.img} style={{ width: "255px" }} />
                                        <Row style={{ justifyContent: 'space-between', maxWidth: 255, marginTop: 10 }}>
                                            <Link>
                                                <p style={{ fontSize: 13, color: "black", }}>{item.title}</p>
                                            </Link>
                                            <Col>
                                                <h3><Money value={item.price} /></h3>
                                            </Col>
                                            <div className='overlayss'></div>
                                            <Button className='add_to_carts'> <ShoppingCartOutlined />Add to cart</Button>
                                        </Row>
                                    </Col>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </Row>
            </Col>

            <CancelOrder open={openCancel} setOpen={setOpenCancel} item={item} getAll={getOrderHistory} />

            <DetailOrder open={openDetail} setOpen={setOpenDetail} item={item} />

            <DetailOrderProducts open={openOrderProducts} setOpen={setOpenOrderProducts} item={item} />
            <Footer />
        </>
    )
}
