import { MailOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Input, Row } from 'antd';
import Link from 'antd/es/typography/Link';
import React from 'react';
import "../../CSS/Login.css";
import "../../CSS/ForgetPass.css";
import Header from '../../components/Header';
import Footer from '../../components/Footer';


export default function ForgetPass() {
    return (
        <>
            <Header />
            <Col className='container' span={24}>
                <Row className='wrapp_form_forget' >
                    <Col className='img_loginForm'>
                        <img className='img_login' src='https://seamaf.com/frontend/img/background.jpg' />
                    </Col>
                    <Col className='form_forgetPass' span={12}>
                        <h2 style={{ textAlign: "center" }}>Forget Password</h2>
                        <form className='form_login'>
                            <Input className='email' size="large" placeholder="Email Address" prefix={<MailOutlined className="custom-mail-icon" />} />
                            <br />
                            <Button className='btn_submit'>Send password Reset Link</Button>
                            <Link style={{ display: 'flex', justifyContent: 'flex-end', marginTop: 10 }}>
                            </Link>
                        </form>
                        <p className='orLogin' >or login with</p>
                        <Row className='warpp_orther'>
                            <Button className='btn_facebook'>Facebook</Button>
                            <Button className='btn_email'>Google</Button>
                        </Row>
                        <Divider />
                        <Col className='bottom_text'>
                            <p>Don't have an account ? <Link href='/register'>Register here</Link></p>
                        </Col>
                    </Col>
                </Row >
            </Col >
            <Footer />
        </>
    )
}
