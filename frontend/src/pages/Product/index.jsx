import { DeleteTwoTone, EditTwoTone, ExportOutlined, EyeTwoTone, PlusOutlined } from '@ant-design/icons'
import { Breadcrumb, Button, Col, Collapse, Grid, Image, Input, Row, Select, Space, Table, Tag, Tooltip } from 'antd'
import React, { useEffect, useState } from 'react'
import AddModal from '../../components/Products/addModal'
import { DeleteProducts } from '../../components/Products/deleteModal'
import UpdateModal from '../../components/Products/updateModal'
import { DetailProduct } from '../../components/Products/detailModal'
import "../../CSS/ProductDash.css"
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import Money from '../../components/Money'
import { getAllProduct } from '../../API/apiRequest'
import * as XLSX from 'xlsx'
import axios from 'axios'



export const Product = () => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const { Panel } = Collapse;
    const navigate = useNavigate();
    const dispatch = useDispatch()




    const [data, setData] = useState([])
    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false)
    const [openUpdate, setOpenUpdate] = useState(false)
    const [openDetail, setOpenDetail] = useState(false)
    const [Item, setItem] = useState();

    // Kích hoạt effect
    const [trigger, setTrigger] = useState(false)

    // Tìm kiếm
    const [titleSearch, setTitleSearch] = useState("");
    const [statuSearch, setStatusSearch] = useState("");
    const [codeSearch, setCodeSearch] = useState("")

    const HandleSearch = async () => {
        try {
            const res = await axios.get(`${baseURL}product/search?title=${titleSearch}&code=${codeSearch}&status=${statuSearch}`)
            setData(res.data)
        } catch (e) {
            console.log("Err search: ", e)
        }
    }

    const resest_filter = () => {
        setTitleSearch("");
        setStatusSearch("");
        setCodeSearch("")
        getAllProducts()
    }


    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const productsData = useSelector((state) => state.product.productList?.products)

    // check role
    const MANAGER = user?.user.Role.includes("MANAGER")
    const STAFF = user?.user.Role.includes("STAFF")

    const accessToken = user?.accessToken


    const getAllProducts = async () => {
        getAllProduct(dispatch, 0, accessToken)
    }

    const handleDelete = (item) => {
        setItem(item)
        setOpenDelete(true)

    }

    const handleUpdate = (item) => {
        setItem(item);
        setOpenUpdate(true)
        setTrigger(!trigger)
    }

    const handleStatusSearch = (value) => {
        setStatusSearch(value)
    }

    const handleDetail = (value) => {
        setOpenDetail(true);
        setItem(value)
    }


    // Phân trang
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 5, // số mục hiển thị trên mỗi trang
    });

    const handleChangePagination = (page, pageSize) => {
        setPagination({
            ...pagination,
            current: page,
            pageSize: pageSize,
        });
    };

    // Xuất file Excel
    const exportToExcel = (data) => {
        const worksheet = XLSX.utils.json_to_sheet(data);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, 'Sheet.xlsx');
    }


    useEffect(() => {
        if (!user) {
            navigate("/");
        }
        if (user?.accessToken) {
            getAllProducts()
        }
    }, []);

    useEffect(() => { setData(productsData.products) }, [productsData])


    const columns = [
        {
            title: 'STT',
            dataIndex: 'id',
            key: 'id',
            width: 100,
            render: (text, record, index) => <span>{index + 1}</span>,
        },
        {
            title: 'Mã sản phẩm',
            dataIndex: 'code',
            key: 'code',
            width: 150,
        },
        {
            title: 'Tên sản phẩm',
            dataIndex: 'title',
            key: 'title',
            render: (text) => <b>{text}</b>,
        },
        {
            title: 'Ảnh sản phẩm',
            dataIndex: 'img',
            key: 'img',
            render: (item) => <Image
                width={70}
                src={item}
            />
        },
        {
            title: 'Giá sản phẩm',
            dataIndex: 'price',
            key: 'price',
            render: (value) => <Money value={value} />
        },
        {
            title: 'Sản phẩm đã bán',
            dataIndex: 'orderCount',
            key: 'orderCount',
            render: (value) => <b style={{ color: '#e83e8c' }}>{value} sản phẩm</b>
        },
        {
            title: 'Tồn kho',
            dataIndex: 'remainItem',
            key: 'remainItem',
            render: (_, record) => {
                return (
                    <b>
                        {record.quantityItem - record.orderCount} sp
                    </b>
                );
            },
        },
        {
            title: 'Mô tả',
            dataIndex: 'description',
            key: 'description',
            width: 350
        },
        {
            title: 'Trạng thái',
            key: 'status',
            dataIndex: 'status',
            render: (_, { status }) => (
                <>
                    <Tag color={status === 'active' ? 'green' : 'red'}>
                        {status === "active" ? "Còn hàng" : "Hết hàng"}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Chức năng',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Tooltip placement="top" title="Chi tiết" onClick={() => handleDetail(record)}>
                        <EyeTwoTone twoToneColor="#531dab" />
                    </Tooltip>
                    {STAFF ? "" : <>
                        <Tooltip placement="top" title="Sửa" onClick={() => handleUpdate(record)}>
                            <EditTwoTone />
                        </Tooltip>
                        <Tooltip placement="top" title="Xóa">
                            <DeleteTwoTone twoToneColor="#f5222d" onClick={() => handleDelete(record)} />
                        </Tooltip>
                    </>}


                </Space>
            ),
        },
    ];


    return (
        <>
            <Breadcrumb
                routes={[
                    { path: '/', breadcrumbName: "Dashboard" },
                    { path: '/managerment', breadcrumbName: 'Quản lý hệ thống' },
                    { path: '/mangerment/drinks', breadcrumbName: 'Sản phẩm' }]}
                separator="/"
                style={{
                    margin: '16px 3px',
                }}
            />
            <Col className='col_wrapp'>
                <Collapse>
                    <Panel header="Tìm kiếm" key="1">
                        <Row style={{ alignItems: 'center' }}>
                            <Col xl={5} sm={24} xs={24} className="input">
                                <Input placeholder="Mã sản phẩm" value={codeSearch} onChange={(e) => setCodeSearch(e.target.value)} />
                            </Col>
                            <Col xl={5} sm={24} xs={24} className="input">
                                <Input placeholder="Tên sản phẩm" value={titleSearch} onChange={(e) => setTitleSearch(e.target.value)} />
                            </Col>
                            <Col xl={5} sm={24} xs={24} style={{ marginTop: 5 }}>
                                <Select
                                    className='select'
                                    style={{ width: '100%' }}
                                    placeholder="Trạng thái"
                                    value={statuSearch}
                                    onChange={handleStatusSearch}

                                >
                                    <Select.Option value="">Tất cả</Select.Option>
                                    <Select.Option value="active">Còn hàng</Select.Option>
                                    <Select.Option value="inactive">Hết hàng</Select.Option>
                                </Select>
                            </Col>
                        </Row>

                        {/* search */}
                        <Row justify="end" style={{ marginTop: 25, gap: 6 }}>
                            <Button type="primary" ghost onClick={HandleSearch}>Tìm kiếm</Button>
                            <Button danger onClick={resest_filter}>Reset bộ lọc</Button>
                        </Row>

                    </Panel>
                </Collapse>
            </Col>
            <Col className='col_wrapp_title' style={{ padding: "30px 0px 10px 0px" }}>
                <Row justify="space-between">
                    <h2>Danh sách sản phẩm <Tag color="#e83e8c">{data?.length}</Tag></h2>
                    {STAFF ? "" : <Row style={{ gap: 6 }}>
                        <Button type="primary" icon={<ExportOutlined />} style={{ background: "#e83e8c" }} onClick={() => exportToExcel(data)} >
                            Xuất file Excel
                        </Button>
                        {MANAGER ? "" : <Button type="primary" icon={<PlusOutlined />} onClick={() => setOpen(true)} style={{ background: "#e83e8c" }} >
                            Thêm mới
                        </Button>}
                    </Row>}




                </Row>
            </Col>

            {/* Table */}
            <Table className='table' columns={columns} dataSource={data} scroll={{ y: 470 }} pagination={{
                current: pagination.current,
                pageSize: pagination.pageSize,
                total: data?.length,
                onChange: handleChangePagination
            }} />

            <AddModal data={open} setData={setOpen} getAll={getAllProducts} />

            <DeleteProducts open={openDelete} setOpen={setOpenDelete} item={Item} getAll={getAllProducts} />

            <UpdateModal data={openUpdate} setData={setOpenUpdate} item={Item} getAll={getAllProducts} trigger={trigger} />

            <DetailProduct open={openDetail} setOpen={setOpenDetail} item={Item} />

        </>
    )
}
