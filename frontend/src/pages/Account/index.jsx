import { DeleteTwoTone, EditTwoTone, ExportOutlined, EyeTwoTone, PlusOutlined, ReloadOutlined, UserOutlined } from '@ant-design/icons'
import { Avatar, Breadcrumb, Button, Col, Collapse, Input, Row, Select, Space, Table, Tag, Tooltip } from 'antd'
import axios from 'axios'
import moment from "moment"
import "../../CSS/ProductDash.css"
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import * as XLSX from 'xlsx'
import { getAllUsers } from '../../API/apiRequest'
import AddModal from '../../components/Account/addModal'
import UpdateModal from '../../components/Account/updateModal'
import { DeleteModal } from '../../components/Account/deleteModal'
import { ResetModal } from '../../components/Account/resetPassModal'
import { getAllRole } from '../../API/apiRequest'




export const Account = () => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const { Panel } = Collapse;
    const navigate = useNavigate();
    const dispatch = useDispatch()

    const [data, setData] = useState([])
    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false)
    const [openUpdate, setOpenUpdate] = useState(false)
    const [openReset, setOpenReset] = useState(false);
    const [itemUser, setItemUser] = useState()
    const [roleFilter, setRoleFiter] = useState([])

    // Kích hoạt effect
    const [trigger, setTrigger] = useState(false);

    // Tìm kiếm
    const [emailSearch, setEmailSearch] = useState("");
    const [statuSearch, setStatusSearch] = useState();
    const [roleSearch, setRoleSearch] = useState();

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const userData = useSelector((state) => state.users.userList?.userData)
    const roleData = useSelector((state) => state.role.roleList?.roleData)

    // check role
    const STAFF = user?.user.Role.includes("STAFF")


    const getDataUser = () => {
        getAllUsers(user?.accessToken, dispatch)
    }

    const getRole = () => {
        getAllRole(user?.accessToken, dispatch)
    }


    useEffect(() => {
        if (!user) {
            navigate("/");
        }
        if (user?.accessToken) {
            getDataUser()
            getRole()
        }
    }, [])

    useEffect(() => {
        setData(userData)
        setRoleFiter(roleData)
    }, [userData, roleData])

    useEffect(() => {
        if (STAFF) {
            navigate("/dashboard/forbidden")
        }
    }, [])



    // Xuất file Excel
    const exportToExcel = (data) => {
        const worksheet = XLSX.utils.json_to_sheet(data);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, 'data.xlsx');
    }
    // Phân trang
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 4, // số mục hiển thị trên mỗi trang
    });

    const handleChangePagination = (page, pageSize) => {
        setPagination({
            ...pagination,
            current: page,
            pageSize: pageSize,
        });
    };

    const handleUpdate = (rc) => {
        setOpenUpdate(true);
        setTrigger(!trigger)
        setItemUser(rc)
    }

    const handleDelete = (record) => {
        setOpenDelete(true)
        setItemUser(record)
    }

    const handleResetPassword = (rc) => {
        setOpenReset(true);
        setItemUser(rc)
    }

    const handleRoleSearch = (value) => {
        setRoleSearch(value)
    }

    const handleStatusSearch = (value) => {
        setStatusSearch(value)
    }

    const handleSearch = async () => {
        try {
            const result = await axios.get(`${baseURL}user/search?email=${emailSearch || ""}&Role=${roleSearch || ""}&status=${statuSearch || ""}`)
            setData(result.data)

        } catch (e) {
            console.log("Err search", e)
        }
    }

    const resest_filter = () => {
        getDataUser();
        setEmailSearch("");
        setRoleSearch();
        setStatusSearch();
    }



    const columns = [
        {
            title: 'STT',
            key: 'stt',
            width: 100,
            render: (text, record, index) => <span>{index + 1}</span>,
        },
        {
            title: 'Tên tài khoản',
            dataIndex: 'username',
            key: 'username',
            width: 350,
            render: (_, value) => <>
                <Row style={{ alignItems: 'center', justifyContent: 'space-between', width: 220 }}>
                    <Col>
                        <span>Tên: <b> {value.username}</b></span>
                        <br />
                        <span>Giới tính: <b>{value.sex}</b></span>
                        <br />
                        {value.address ? <span>Địa chỉ: <b>{value.address}</b></span> : null}
                        <p>Ngày sinh: <b>{moment(value.birthday).format('DD/MM/YYYY')}</b></p>
                    </Col>
                    <Col style={{ marginBottom: 15 }}>
                        {value.avatar ? <Avatar src={value.avatar} size={50} /> : <Avatar icon={<UserOutlined />} size={50} />}
                    </Col>
                </Row>

            </>
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            width: 300,
            render: (email) => <b><i>{email}</i></b>
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Nhóm quyền',
            dataIndex: 'Role',
            key: 'Role',
            render: (role) => (
                <>
                    <Tag color={role === "ADMIN" ? '#f5222d' : role === "STAFF" ? "#d3adf7" : role === "MANAGER" ? '#096dd9' : '#595959'}>{role}</Tag>
                </>
            )
        },
        {
            title: 'Trạng thái',
            key: 'status',
            dataIndex: 'status',
            render: (_, { status }) => (
                <>
                    <Tag color={status === 'active' ? 'green' : 'red'}>
                        {status === "active" ? "Kích hoạt" : "Chưa kích hoạt"}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Chức năng',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Tooltip placement="top" title="Chi tiết">
                        <EyeTwoTone twoToneColor="#531dab" />
                    </Tooltip>
                    <Tooltip placement="top" title="Sửa" onClick={() => handleUpdate(record)}>
                        <EditTwoTone />
                    </Tooltip>
                    <Tooltip placement="top" title="Xóa" onClick={() => { handleDelete(record) }} >
                        <DeleteTwoTone twoToneColor="#f5222d" />
                    </Tooltip>
                    <Tooltip placement="top" title="reset mật khẩu" onClick={() => handleResetPassword(record)}>
                        <ReloadOutlined />
                    </Tooltip>
                </Space>
            ),
        },
    ];


    return (
        <>
            <Breadcrumb
                routes={[
                    { path: '/', breadcrumbName: "Home" },
                    { path: '/managerment', breadcrumbName: 'Quản lý tài khoản' },
                    { path: '/mangerment/account', breadcrumbName: 'Tài khoản' }]}
                separator="/"
                style={{
                    margin: '16px 3px',
                }}
            />
            <Col className='col_wrapp'>
                <Collapse>
                    <Panel header="Tìm kiếm" key="1">
                        <Row style={{gap: 6, alignItems: 'center'}}>
                            <Col xl={6} sm={24} xs={24} className="inputss">
                                <Input value={emailSearch} onChange={(e) => setEmailSearch(e.target.value)} placeholder="Nhập Email" />
                            </Col>
                            <Col xl={6} sm={24} xs={24}>
                                <Select
                                    className='select'
                                    placeholder="Nhóm quyền"
                                    style={{width:'100%'}}
                                    value={roleSearch}
                                    onChange={handleRoleSearch}
                                >
                                    {roleFilter?.map((item) =>
                                        <Select.Option key={item._id} value={item.roleName}>{item.roleName}</Select.Option>
                                    )}
                                </Select>
                            </Col>
                            <Col xl={6} sm={24} xs={24}>
                                <Select
                                    className='select'
                                    placeholder="Trạng thái"
                                    value={statuSearch}
                                    onChange={handleStatusSearch}
                                    style={{ width: '100%' }}

                                >
                                    <Select.Option value="active">Kích hoạt</Select.Option>
                                    <Select.Option value="inactive">Chưa kích hoạt</Select.Option>
                                </Select>
                            </Col>
                        </Row>

                        {/* search */}
                        <Row justify="end" style={{ marginTop: "25px", gap: 6 }}>
                            <Button type="primary" ghost  onClick={handleSearch} >Tìm kiếm</Button>
                            <Button danger onClick={resest_filter} >Reset bộ lọc</Button>
                        </Row>

                    </Panel>
                </Collapse>
            </Col>
            <Col className='col_wrapp_title' style={{ padding: "30px 0px 10px 0px" }}>
                <Row justify="space-between">
                    <h2>Danh sách Tài khoản <Tag color="#e83e8c">{data?.length}</Tag></h2>
                    <Row style={{ gap: 6 }}>
                        <Button type="primary" icon={<ExportOutlined />} style={{background: "#e83e8c" }} onClick={() => exportToExcel(data)}>
                            Xuất file Excel
                        </Button>
                        <Button type="primary" icon={<PlusOutlined />} onClick={() => setOpen(true)} style={{ background: "#e83e8c" }}>
                            Thêm mới
                        </Button>

                    </Row>

                </Row>
            </Col>
            {/* Table */}
            <Table className='table' columns={columns} dataSource={data} scroll={{ y: 480 }} pagination={{
                current: pagination.current,
                pageSize: pagination.pageSize,
                total: data?.length,
                onChange: handleChangePagination
            }} />

            <AddModal data={open} setData={setOpen} getAll={getDataUser} roleFilter={roleFilter} />

            <UpdateModal data={openUpdate} setData={setOpenUpdate} item={itemUser} getAll={getDataUser} trigger={trigger} roleFilter={roleFilter} />

            <DeleteModal open={openDelete} setOpen={setOpenDelete} item={itemUser} getAll={getDataUser} />

            <ResetModal open={openReset} setOpen={setOpenReset} item={itemUser} getAll={getDataUser} />
        </>
    )
}
