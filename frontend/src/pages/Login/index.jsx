import React, { useEffect, useState } from 'react'
import { Button, Checkbox, Col, Divider, Input, Row } from 'antd'
import { UserOutlined, KeyOutlined } from '@ant-design/icons';
import "../../CSS/Login.css"
import Link from 'antd/es/typography/Link';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { LoginUser } from '../../API/apiRequest';
import Header from '../../components/Header';
import Footer from '../../components/Footer';


export default function Login() {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [remember, setRemember] = useState(false);

    const handleLogin = (e) => {
        e.preventDefault();
        const newUser = {
            username: username,
            password: password
        }
        LoginUser(newUser, dispatch, navigate)
        if (remember) {
            // Lưu vào localStorage
            const encryptedPassword = btoa(password); // Mã hóa mật khẩu
            localStorage.setItem("password", encryptedPassword);
            localStorage.setItem("username", username);
        }
    }

    useEffect(() => {
        const storedPassword = localStorage.getItem("password");
        const storedUsername = localStorage.getItem("username");

        if (storedPassword) {
            const decryptedPassword = atob(storedPassword); // Giải mã mật khẩu

            setPassword(decryptedPassword);
            setUsername(storedUsername);
            setRemember(true);
        }
    }, [])




    return (
        <>
            <Header />
            <Col className='container' span={24}>
                <Row className='wrapp_form' >
                    <Col className='img_loginForm' xs={0} md={0} lg={0} xl={6}>
                        <img className='img_login' src='https://i.pinimg.com/564x/90/a2/1d/90a21d03c76b1bf220e339252fb664c9.jpg' />
                    </Col>
                    <Col className='form' md={25}>
                        <h2 style={{ textAlign: "center" }}>Login to your account</h2>
                        <form className='form_login' onSubmit={handleLogin}>
                            <Input className='email' type='text' value={username} onChange={(e) => setUsername(e.target.value)} size="large" placeholder="Username" prefix={< UserOutlined className="custom-mail-icon" />} />
                            <Input.Password className='password' type='password' value={password} onChange={(e) => setPassword(e.target.value)} size="large" placeholder="Password" prefix={<KeyOutlined className="custom-mail-icon" />} />
                            <Checkbox className='remember' checked={remember} onChange={(e) => setRemember(e.target.checked)} style={{ fontSize: 15, margin: "20px 0px" }}>
                                <b>Remember me</b>
                            </Checkbox>
                            <br />
                            <Button className='btn_submit' onClick={handleLogin}>Login</Button>
                            <Link style={{ display: 'flex', justifyContent: 'flex-end', marginTop: 10 }} href='/forget-password?'>
                                <b style={{ color: 'black', fontSize: 15 }}>Forget password ?</b>
                            </Link>
                        </form>
                        <p className='orLogin' >or login with</p>
                        <Row className='warpp_orther'>
                            <Button className='btn_facebook'>Facebook</Button>
                            <Button className='btn_email'>Google</Button>
                        </Row>
                        <Divider />
                        <Col className='bottom_text'>
                            <p>Check out as guest ? <Link>Click here</Link></p>
                            <p>Don't have an account ? <Link href='/register'>Register here</Link></p>
                        </Col>
                    </Col>
                </Row >
            </Col >
            <Footer />
        </>
    )
}
