import { DeleteTwoTone, EditTwoTone, ExportOutlined, EyeTwoTone, PlusOutlined } from '@ant-design/icons'
import { Breadcrumb, Button, Col, Collapse, Input, Row, Select, Space, Table, Tag, Tooltip } from 'antd'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import * as XLSX from 'xlsx'
import { getAllRole } from '../../API/apiRequest'
import AddModal from '../../components/Role/addModal'
import UpdateModal from '../../components/Role/updateModal'
import { DeleteModal } from '../../components/Role/deleteModal'
import "../../CSS/ProductDash.css"



export const Role = () => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const { Panel } = Collapse;
    const navigate = useNavigate();
    const dispatch = useDispatch()


    const [data, setData] = useState([])
    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false)
    const [openUpdate, setOpenUpdate] = useState(false)
    const [openDetail, setOpenDetail] = useState(false)
    const [Item, setItem] = useState();

    // Kích hoạt effect
    const [trigger, setTrigger] = useState(false)

    // Tìm kiếm
    const [statuSearch, setStatusSearch] = useState();
    const [roleSearch, setRoleSearch] = useState()

    const HandleSearch = async () => {
        try {
            const res = await axios.get(`${baseURL}role/search?&roleName=${roleSearch || ""}&status=${statuSearch || ""}`)
            setData(res.data)
        } catch (e) {
            console.log("Err search: ", e)
        }
    }

    const resest_filter = () => {
        setStatusSearch();
        setRoleSearch();
        getRole()
    }


    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const roleData = useSelector((state) => state.role.roleList?.roleData)

    // check role
    const MANAGER = user?.user.Role.includes("MANAGER");
    const STAFF = user?.user.Role.includes("STAFF")

    const accessToken = user?.accessToken


    const getRole = async () => {
        getAllRole(accessToken, dispatch)
    }

    const handleDelete = (item) => {
        setItem(item)
        setOpenDelete(true)

    }

    const handleUpdate = (item) => {
        setItem(item);
        setOpenUpdate(true)
        setTrigger(!trigger)
    }

    const handleStatusSearch = (value) => {
        setStatusSearch(value)
    }

    const handleRoleSearch = (value) => {
        setRoleSearch(value)
    }


    // Phân trang
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 5, // số mục hiển thị trên mỗi trang
    });

    const handleChangePagination = (page, pageSize) => {
        setPagination({
            ...pagination,
            current: page,
            pageSize: pageSize,
        });
    };

    // Xuất file Excel
    const exportToExcel = (data) => {
        const worksheet = XLSX.utils.json_to_sheet(data);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Data');
        XLSX.writeFile(workbook, 'Sheet.xlsx');
    }

    useEffect(() => {
        if (!user) {
            navigate("/");
        }
        if (STAFF) {
            navigate("/dashboard/forbidden")
        }

        if (user?.accessToken) {
            getRole()
        }
    }, []);

    useEffect(() => { setData(roleData) }, [roleData])
    useEffect(() => {
        if (MANAGER) {
            navigate("/dashboard/forbidden")
        }
    }, [])


    const columns = [
        {
            title: 'STT',
            dataIndex: 'id',
            key: 'id',
            // width: 100,
            render: (text, record, index) => <span>{index + 1}</span>,
        },
        {
            title: "Tên quyền",
            dataIndex: "roleName",
            key: "roleName",
            render: (role) => (
                <>
                    <Tag color={role === "ADMIN" ? '#f5222d' : role === "STAFF" ? "#d3adf7" : role === "MANAGER" ? '#096dd9' : '#595959'}>{role}</Tag>
                </>
            )
        },
        {
            title: 'Mô tả',
            dataIndex: 'description',
            key: 'description',
            // width: 550
        },
        {
            title: 'Trạng thái',
            key: 'status',
            dataIndex: 'status',
            render: (_, { status }) => (
                <>
                    <Tag color={status === 'active' ? 'green' : 'red'}>
                        {status === "active" ? "Kích hoạt" : "Chưa kích hoạt"}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Chức năng',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Tooltip placement="top" title="Chi tiết">
                        <EyeTwoTone twoToneColor="#531dab" />
                    </Tooltip>
                    <Tooltip placement="top" title="Sửa" onClick={() => handleUpdate(record)}>
                        <EditTwoTone />
                    </Tooltip>
                    <Tooltip placement="top" title="Xóa">
                        <DeleteTwoTone twoToneColor="#f5222d" onClick={() => handleDelete(record)} />
                    </Tooltip>
                </Space>
            ),
        },
    ];


    return (
        <>
            <Breadcrumb
                routes={[
                    { path: '/', breadcrumbName: "Dashboard" },
                    { path: '/dashboard', breadcrumbName: 'Quản lý hệ thống' },
                    { path: '/dashboard/role', breadcrumbName: 'Quyền' }]}
                separator="/"
                style={{
                    margin: '16px 3px',
                }}
            />
            <Col className='col_wrapp'>
                <Collapse>
                    <Panel header="Tìm kiếm" key="1">
                        <Row style={{ alignItems: 'center', gap:6 }}>
                            <Col xl={6} sm={24} xs={24} className="inputs">
                                <Select
                                    className='select'
                                    placeholder="Nhóm quyền"
                                    value={roleSearch}
                                    style={{width:'100%'}}
                                    onChange={handleRoleSearch}

                                >
                                    <Select.Option value="ADMIN">ADMIN</Select.Option>
                                    <Select.Option value="STAFF">STAFF</Select.Option>
                                    <Select.Option value="MANAGER">MANAGER</Select.Option>
                                </Select>
                            </Col>
                            <Col xl={6} sm={24} xs={24}>
                                <Select
                                    className='select'
                                    placeholder="Trạng thái"
                                    style={{ width: '100%' }}
                                    value={statuSearch}
                                    onChange={handleStatusSearch}

                                >
                                    <Select.Option value="active">Kích hoạt</Select.Option>
                                    <Select.Option value="inactive">Chưa kích hoạt</Select.Option>
                                </Select>
                            </Col>
                        </Row>

                        {/* search */}
                        <Row justify="end" style={{ marginTop: 25, gap: 6 }}>
                            <Button type="primary" ghost onClick={HandleSearch}>Tìm kiếm</Button>
                            <Button danger onClick={resest_filter}>Reset bộ lọc</Button>
                        </Row>

                    </Panel>
                </Collapse>
            </Col>
            <Col className='col_wrapp_title' style={{ padding: "30px 0px 10px 0px" }}>
                <Row justify="space-between">
                    <h2>Danh sách các quyền <Tag color="#e83e8c">{data?.length}</Tag></h2>

                    <Row style={{gap: 6}}>
                        <Button type="primary" icon={<ExportOutlined />} style={{background: "#e83e8c" }} onClick={() => exportToExcel(data)} >
                            Xuất file Excel
                        </Button>
                        <Button type="primary" icon={<PlusOutlined />} onClick={() => setOpen(true)} style={{ background: "#e83e8c" }} >
                            Thêm mới
                        </Button>
                    </Row>



                </Row>
            </Col>

            {/* Table */}
            <Table className='table' columns={columns} dataSource={data} scroll={{ y: 470 }} pagination={{
                current: pagination.current,
                pageSize: pagination.pageSize,
                total: data?.length,
                onChange: handleChangePagination
            }} />

            <AddModal data={open} setData={setOpen} getAll={getRole} />

            <UpdateModal data={openUpdate} setData={setOpenUpdate} item={Item} getAll={getRole} trigger={trigger} />

            <DeleteModal open={openDelete} setOpen={setOpenDelete} item={Item} getAll={getRole} />

        </>
    )
}
