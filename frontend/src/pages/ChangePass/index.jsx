import { KeyOutlined, LockOutlined, UserOutlined, UnlockOutlined } from '@ant-design/icons';
import { Button, Col, Divider, Form, Input, Row } from 'antd';
import Link from 'antd/es/typography/Link';
import React, { useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import "../../CSS/Login.css";
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import axios from 'axios';
import { toast } from 'react-toastify';


export default function ChangePass() {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    console.log(user.user?.username)

    const navigate = useNavigate()
    const formRef = useRef(null)


    const [userName, setUsername] = useState(user.user?.username);
    const [password, setPassword] = useState(user.user?.password);
    const [newPasswords, setNewPassword] = useState('');
    const [confirm, setConfirm] = useState("")



    const handleChangePass = () => {
        formRef.current.validateFields().then(async () => {
            if (newPasswords === confirm) {
                try {
                    const newPassword = newPasswords;
                    await axios.put(`${baseURL}user/changePassword/${user.user?._id}`, { newPassword });
                    toast.success("Đổi mật khẩu thành công !")
                    navigate("/")
                } catch (e) {
                    console.log("Err:", e)
                }
            } else {
                toast.warning("Xác nhận không khớp !")
            }
        })
    }

    return (
        <>
            <Header />
            <Col className='container' span={24}>
                <Row className='wrapp_form' >
                    <Col className='img_loginForm' xs={0} md={0} lg={0} xl={6}>
                        <img className='img_login' src='https://i.pinimg.com/564x/90/a2/1d/90a21d03c76b1bf220e339252fb664c9.jpg' />
                    </Col>
                    <Col className='form' xs={24} sm={24} md={12}>
                        <Form ref={formRef} onSubmit={handleChangePass}>
                            <h2 style={{ textAlign: "center" }}>Change to password</h2>
                            <form className='form_login'>
                                <Form.Item
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Xin vui lòng nhập tên',
                                        },
                                    ]}
                                >
                                    <Input className='emails'
                                        type='text'
                                        value={userName}
                                        onChange={(e) => setUsername(e.target.value)}
                                        size="large"
                                        placeholder="Username"
                                        prefix={< UserOutlined className="custom-mail-icon" />}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Xin vui lòng nhập mật khẩu',
                                        },
                                    ]}
                                >
                                    <Input.Password className='password'
                                        type='password'
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        size="large"
                                        placeholder="Password"
                                        prefix={<KeyOutlined className="custom-mail-icon" />}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name="new-password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Xin vui lòng nhập mật khẩu mới',
                                        },
                                    ]}
                                >
                                    <Input.Password className='password'
                                        type='password' value={newPasswords}
                                        onChange={(e) => setNewPassword(e.target.value)}
                                        size="large"
                                        placeholder="New Password"
                                        prefix={<UnlockOutlined className="custom-mail-icon" />}
                                    />
                                </Form.Item>
                                <Form.Item
                                    name="cofirm"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Xin vui lòng xác nhận mật khẩu',
                                        },
                                    ]}
                                >
                                    <Input.Password className='password'
                                        type='password' value={confirm}
                                        onChange={(e) => setConfirm(e.target.value)}
                                        size="large"
                                        placeholder="Confirm "
                                        prefix={<LockOutlined className="custom-mail-icon" />}
                                    />
                                </Form.Item>
                                <br />
                                <Button className='btn_submit' onClick={handleChangePass}>Change password</Button>
                            </form>
                            <p className='orLogin' >or login with</p>
                            <Row className='warpp_orther'>
                                <Button className='btn_facebook'>Facebook</Button>
                                <Button className='btn_email'>Google</Button>
                            </Row>
                            <Divider />
                            <Col className='bottom_text'>
                                <p>Check out as guest ? <Link>Click here</Link></p>
                                <p>Don't have an account ? <Link href='/register'>Register here</Link></p>
                            </Col>
                        </Form>

                    </Col>
                </Row >
            </Col >
            <Footer />
        </>
    )
}
