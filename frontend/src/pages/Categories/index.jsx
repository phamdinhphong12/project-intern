import { Breadcrumb, Button, Col, Divider, Grid, Input, Pagination, Row, Select } from 'antd'
import React, { useEffect, useState } from 'react'
import "../../CSS/Categories.css"
import { Link, useNavigate } from 'react-router-dom'
import ProductItem from '../../components/ProductItem';
import { useDispatch } from 'react-redux'
import { useSelector } from 'react-redux'
import { getAllProduct } from '../../API/apiRequest'
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import axios from 'axios';
import { toast } from 'react-toastify';


const { useBreakpoint } = Grid;

export default function Categories() {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const navigate = useNavigate()
    const { Search } = Input;
    const dispatch = useDispatch()
    const [data, setData] = useState([]);
    const [searchTitle, setSearchTitle] = useState("")
    const [selectedSort, setSelectedSort] = useState();
    const screens = useBreakpoint();

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const productsData = useSelector((state) => state.product.productList?.products)

    const accessToken = user?.accessToken

    // phân trang
    const [currentPage, setCurrentPage] = useState(productsData.page);
    const [totalPages, setTotalPages] = useState(productsData.totalPages);
    const pageSize = productsData.limit;

    const handlePageChange = async (page) => {
        setCurrentPage(page);
        navigate(`/categories?page=${page}`);
        const res = await getAllProduct(dispatch, page, accessToken);
        setData(res.products);
        setTotalPages(res.totalPages);
    };


    const getAllProducts = async () => {
        getAllProduct(dispatch, 1, accessToken)
    }

    const searchNameProduct = async () => {
        try {
            const res = await axios.get(`${baseURL}product/search?title=${searchTitle}&code=${""}&status=${""}`)
            setData({ products: res.data })
            if (res.data.length === 0) {
                toast.warning('Không có sản phẩm phù hợp !');
            }
        } catch (e) {
            console.log("Err search: ", e)
        }
    }

    const handleSearchTitle = () => {
        searchNameProduct()
    }

    // sắp xếp price
    const SortItem = (item, data, setData) => {
        let sortedData = [...data.products]; // Tạo một bản sao của mảng sản phẩm để không làm thay đổi dữ liệu gốc

        switch (item) {
            case "increase_price":
                sortedData.sort((a, b) => a.price - b.price);
                setSelectedSort(item);
                break;
            case "descrease_price":
                sortedData.sort((a, b) => b.price - a.price);
                setSelectedSort(item);
                break;
            default:
                break;
        }

        setData({ ...data, products: sortedData }); // Cập nhật lại dữ liệu với mảng đã sắp xếp
    };


    const resest_filter = () => {
        getAllProducts()
        setSearchTitle("")
        setSelectedSort()
        setCurrentPage(1)
    }

    useEffect(() => {
        if (accessToken) {
            getAllProducts()
        }
    }, [])

    useEffect(() => {
        if (searchTitle === "") {
            getAllProducts()
        }
    }, [searchTitle])

    useEffect(() => {
        setData(productsData)
    }, [productsData])
    return (
        <>
            <Header />
            <Col className='wrappe_cate'>
                <Col span={24} className='header_cate'>
                    <h2>CATEGORY PAGE</h2>
                    <Breadcrumb
                        routes={[
                            { path: '/', breadcrumbName: "Home" },
                            { path: '/categories', breadcrumbName: 'sản phẩm' }]}
                        separator="/"
                        style={{
                            fontSize: 15,
                            fontWeight: 'bold'
                        }}
                    />
                </Col>
                <Col className='wrappe_content'>
                    <Row
                        style={{
                            justifyContent: 'center',
                            padding: (screens.xxl) ? '0 320px' : (screens.xl) ? '0 200px' : 0
                        }}
                    >
                        <Col className='side_bar' span={screens.xxl ? 4 : screens.xl ? 4 : screens.sm ? 12 : 24} >
                            <h3 className='side_bar_title'>CATEGORIES</h3>
                            <Col className='side_bar_link' >
                                <Col>
                                    <Link className='side_bar_link'>Bread</Link>
                                    <Divider />
                                </Col>
                                <Col>
                                    <Link className='side_bar_link'>Finding</Link>
                                    <Divider />
                                </Col>
                                <Col>
                                    <Link className='side_bar_link'>Chain</Link>
                                    <Divider />
                                </Col>
                                <Col>
                                    <Link className='side_bar_link'>Test</Link>
                                    <Divider />
                                </Col>
                                <Col>
                                    <Link className='side_bar_link'>Bags</Link>
                                    <Divider />
                                </Col>
                                <Col>
                                    <Link className='side_bar_link'>Jewelry</Link>
                                    <Divider />
                                </Col>
                            </Col>
                            <Col style={{ marginTop: 150 }}>
                                <h3 className='side_bar_title'>REFINE BY</h3>
                                <Row className='quatity_all'>
                                    <Search
                                        placeholder="Enter name"
                                        value={searchTitle}
                                        onChange={(e) => setSearchTitle(e.target.value)}
                                        onSearch={handleSearchTitle}
                                        style={{
                                            width: 180,
                                        }}
                                    />
                                </Row>
                                <Col className='quatity_all'>
                                    <Select
                                        placeholder="Product price"
                                        style={{ width: 180 }}
                                        value={selectedSort}
                                        onChange={(e) =>
                                            SortItem(e, data, setData)
                                        }

                                    >
                                        <Select.Option value="increase_price">Từ thấp đến cao</Select.Option>
                                        <Select.Option value="descrease_price">Từ cao đến thấp</Select.Option>
                                    </Select>
                                </Col>
                            </Col>

                            <Button className='btn_filter' onClick={resest_filter}>RESET FILTER</Button>
                            <Divider />
                        </Col>
                        <Col span={18}>
                            <Row style={{ justifyContent: "space-between", marginBottom: '20px', flexWrap: 'wrap' }}>
                                {data.products?.map((item) => {
                                    return (
                                        <ProductItem key={item.id} item={item} span={screens.xxl ? 8 : screens.xl ? 10 : screens.md ? 10 : 0} />
                                    )
                                })}
                            </Row>
                            <Pagination
                                current={currentPage}
                                total={totalPages * pageSize}
                                pageSize={pageSize}
                                onChange={handlePageChange}
                            />
                        </Col>
                    </Row>

                </Col>
            </Col>
            <Footer />
        </>
    )
}
