import { DeleteTwoTone, EditTwoTone, ExportOutlined, EyeTwoTone, ContainerOutlined, SolutionOutlined, CheckSquareOutlined, EditOutlined } from '@ant-design/icons';
import { Avatar, Breadcrumb, Button, Col, Collapse, Input, Row, Select, Space, Table, Tabs, Tag, Tooltip } from 'antd';
import axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import * as XLSX from 'xlsx';
import { getAllOrders } from '../../API/apiRequest';
import Money from '../../components/Money';
import { DeleteOrder } from '../../components/Order/deleteModal';
import { UpdateOrder } from '../../components/Order/updateModal';
import { DetailOrder } from '../../components/Order/detailModal';
import { DetailOrderProducts } from '../../components/Order/detailOrderPro';
import "../../CSS/Tabs.css"

export const Orders = () => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const { Panel } = Collapse;
    const navigate = useNavigate();
    const dispatch = useDispatch()


    const [data, setData] = useState([])
    const [dataPending, setDataPending] = useState([]);
    const [dataHistory, setDataHistory] = useState([])
    const [openDelete, setOpenDelete] = useState(false);
    const [openDetail, setOpenDetail] = useState(false);
    const [openUpdate, setOpenUpdate] = useState(false);
    const [openDetailProduct, setOpenDetailProduct] = useState(false)
    const [itemOrder, setItemOrder] = useState()
    // console.log(itemOrder, "detail")
    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser);
    const orderList = useSelector((state) => state.orders.orderList?.orderData)

    // check Role
    const STAFF = user?.user.Role.includes("STAFF")

    // const axiosJWT = createAxios(user, dispatch, loginSuccess)

    // search
    const [nameSearch, setNameSearch] = useState("");
    const [orderPaySearch, setOrderPaySearch] = useState();
    const [orderStatusSearch, setOrderStatusSearch] = useState();

    const handleSearchPay = (value) => {
        setOrderPaySearch(value)
    }

    const handleSearchStatus = (value) => {
        setOrderStatusSearch(value)

    }

    // Phân trang
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 4, // số mục hiển thị trên mỗi trang
    });

    const handleChangePagination = (page, pageSize) => {
        setPagination({
            ...pagination,
            current: page,
            pageSize: pageSize,
        });
    };

    const handleSearch = async () => {
        try {
            const res = await axios.get(`${baseURL}order/search?customer_name=${nameSearch || ""}&order_pay=${orderPaySearch || ""}&order_status=${orderStatusSearch || ''}`)
            setData(res.data)
            setDataHistory([]);
            setDataPending([])

        } catch (e) {
            console.log("Err search: ", e)
        }
    }

    const resest_filter = () => {
        getOrder();
        setNameSearch("");
        setOrderPaySearch();
        setOrderStatusSearch();
    }

    const transformedData = data?.map((order) => {
        const orderProducts = order.order_products.map((product) => {
            const productItems = product.item.map((item) => item.name).join(", ");
            return {
                product_items: productItems,
                product_quantity: product.quantity,
                product_price: product.price,
                customer_name: order.customer_name,
                customer_phone: order.customer_phone,
                customer_address: order.customer_address,
                order_pay: order.order_pay,
                order_description: order.order_description,
                order_status: order.order_status.join(", "),
                created_at: order.createdAt,
                updated_at: order.updatedAt,
            };
        });
        return orderProducts;
    }).flat();



    // Xuất file Excel
    const exportToExcel = (data) => {
        const worksheet = XLSX.utils.json_to_sheet(data);
        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Order');
        XLSX.writeFile(workbook, 'Order.xlsx');
    }

    const handleDeleteOrder = (value) => {
        setOpenDelete(true);
        setItemOrder(value)
    }

    const handleUpdateOrder = (value) => {
        setOpenUpdate(true);
        setItemOrder(value)
    }

    const handleDetailOrder = (value) => {
        setOpenDetail(true);
        setItemOrder(value)
    }

    const getOrder = async () => {
        getAllOrders(0, dispatch)
    }

    const handleSeeProducts = (value) => {
        setItemOrder(value);
        setOpenDetailProduct(true)
    }

    useEffect(() => {
        if (!user) {
            navigate("/")
        }
        // if (user?.accessToken) {
        //     getOrder()
        // }
        getOrder()
    }, []);

    useEffect(() => {
        setData(orderList)
        const orderPending = orderList.filter((item) => {
            if (item.order_status.includes("order")) {
                return item
            }
        })
        setDataPending(orderPending);

        const orderHistory = orderList.filter((item) => {
            if (item.order_status.includes("success") || item.order_status.includes("cancel")) {
                return item
            }
        })
        setDataHistory(orderHistory)
    }, [orderList])



    const columns = [
        {
            title: 'STT',
            key: 'stt',
            width: 70,
            render: (text, record, index) => <span>{(pagination.current - 1) * pagination.pageSize + index + 1}</span>,
        },
        {
            title: 'Thông tin người đặt',
            dataIndex: 'customer_name',
            key: 'customer_name',
            width: 300,
            render: (_, value) => <>
                <Row style={{ alignItems: 'center', justifyContent: 'space-between', width: 220 }}>
                    <Col>
                        <span>Tên: <b>{value.customer_name}</b></span>
                        <br />
                        <span>SĐT: <b>{value.customer_phone}</b></span>
                        <br />
                        <span>Địa chỉ: <b>{value.customer_address}</b></span>
                    </Col>
                </Row>

            </>
        },
        {
            title: 'Các sản phẩm đã đặt',
            dataIndex: 'order_products',
            key: 'order_products',
            width: 300,
            render: (_, record) => (
                <Button onClick={() => { handleSeeProducts(record) }} style={{ backgroundColor: "#e83e8c", color: "white", border: "none" }}>
                    {record.order_products[0].item.length} sản phẩm
                </Button>
                // <>
                //     {value.map((orderProduct, index) => (
                //         <Col key={index}>
                //             {orderProduct.item.map((item, idx) => (
                //                 <>
                //                     <Col key={idx}>
                //                         <span>Tên sản phẩm: <b style={{ color: '#e83e8c' }}>{item.name}</b> </span>
                //                         <br />
                //                         <span>Kích cỡ: <b>{item.size?.join(', ')}</b></span>
                //                         <br />
                //                         <span>Số lượng: <b>{item.quantity}</b></span>
                //                         <br />
                //                         <span>Ghi chú sản phẩm: <b>{item.note}</b></span>
                //                     </Col>
                //                     <br />
                //                 </>

                //             ))}
                //             <br />
                //             <b>Tổng số lượng: <b style={{ color: '#e83e8c' }}>{orderProduct.quantity}</b></b>
                //             <br />
                //             <b>Thành tiền:  <Tag color='green'><Money value={orderProduct.price} /></Tag></b>
                //         </Col>

                //     ))
                //     }
                // </>
            ),
        },
        {
            title: 'Ảnh sản phẩm',
            dataIndex: 'order_products',
            key: 'order_products',
            width: 250,
            render: (value) => (
                <Avatar.Group size="large" maxCount={3} >
                    {value.map((orderProduct) => (
                        <>
                            {orderProduct.item.map((item, idx) => (
                                <Avatar key={idx} src={item.avatar} />
                            ))}
                        </>
                    ))}
                </Avatar.Group>
            )
        },
        {
            title: 'Thời gian',
            dataIndex: 'createdAt',
            key: 'createdAt',
            width: 180,
            render: (value) => (
                <Row style={{ alignItems: 'center', justifyContent: 'space-between', width: 220 }}>
                    <Col>
                        <span>Ngày: <b>{moment(value).format('YYYY-MM-DD')}</b></span>
                        <br />
                        <span>Giờ: <b>{moment(value).format('HH:mm:ss')}</b></span>
                    </Col>
                </Row>
            )
        },
        {
            title: 'Loại thanh toán',
            dataIndex: 'order_pay',
            key: 'order_pay',
            render: (value) => (
                <>
                    <Tag color={value === "1" ? 'green' : 'blue'}>
                        {value === "1" ? "Tiền mặt" : "Banking"}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Trạng thái đơn',
            key: 'order_status',
            dataIndex: 'order_status',
            render: (value) => (
                value.map((item, idx) =>
                    <Tag key={idx} color={item === "order" || item === "cancel" ? "red" : "purple"}>
                        {item === "order" ? "ĐANG XỬ LÝ. . ." : item === "cancel" ? "ĐƠN ĐÃ HUỶ" : "THÀNH CÔNG"}
                    </Tag>
                )
            )
        },
        {
            title: 'Chức năng',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Tooltip placement="top" title="Chi tiết" >
                        <EyeTwoTone twoToneColor="#531dab" onClick={() => handleDetailOrder(record)} />
                    </Tooltip>

                    {STAFF ? '' : <>
                        <Tooltip placement="top" title="Sửa">
                            <EditTwoTone onClick={() => handleUpdateOrder(record)} />
                        </Tooltip>
                        <Tooltip placement="top" title="Xóa">
                            <DeleteTwoTone twoToneColor="#f5222d" onClick={() => handleDeleteOrder(record)} />
                        </Tooltip>
                    </>}


                </Space>
            ),
        },
    ];



    return (
        <>
            <Breadcrumb
                routes={[
                    { path: '/', breadcrumbName: 'Home' },
                    { path: '/', breadcrumbName: 'Đơn hàng' },
                    { path: '/order', breadcrumbName: 'Quản lý đơn hàng' },
                ]}
                separator="/"
                style={{
                    margin: '16px 0',
                }}
            />
            <Col className='col_wrapp'>
                <Collapse>
                    <Panel header="Tìm kiếm" key="1">
                        <Row style={{gap:6}}>
                            <Col xl={5} sm={24} xs={24} className="inputs">
                                <Input value={nameSearch} onChange={(e) => setNameSearch(e.target.value)} placeholder="Nhập tên khách hàng" />
                            </Col>
                            <Col xl={5} sm={24} xs={24} className="inputs">
                                <Select
                                    className='select'
                                    style={{ width: '100%' }}
                                    placeholder="Hình thức thanh toán"
                                    value={orderPaySearch}
                                    onChange={handleSearchPay}
                                >
                                    <Select.Option value="1">Tiền mặt</Select.Option>
                                    <Select.Option value="2">Banking</Select.Option>
                                </Select>
                            </Col>
                            <Col xl={5} sm={24} xs={24} className="inputs">
                                <Select
                                    className='select'
                                    style={{ width: '100%' }}
                                    placeholder="Trạng thái đơn"
                                    value={orderStatusSearch}
                                    onChange={handleSearchStatus}
                                >
                                    <Select.Option value="order">Đang xử lý</Select.Option>
                                    <Select.Option value="success">Đã giao hàng</Select.Option>
                                    <Select.Option value="cancel">Đơn hàng bị hủy</Select.Option>
                                </Select>
                            </Col>
                        </Row>

                        {/* search */}
                        <Row justify="end" style={{ marginTop: "25px", gap:6 }}>
                            <Button type="primary" ghost onClick={handleSearch}>Tìm kiếm</Button>
                            <Button danger onClick={resest_filter}>Reset bộ lọc</Button>
                        </Row>

                    </Panel>
                </Collapse>
            </Col>

            <Col className='col_wrapp_title' style={{ padding: "30px 0px 10px 0px" }}>
                <Row justify="space-between">
                    <h2>Danh sách đơn đặt hàng <Tag color="#e83e8c">{data?.length}</Tag></h2>
                    <Row>
                        <Button type="primary" icon={<ExportOutlined />} style={{ marginRight: "10px", background: '#e83e8c' }} onClick={() => exportToExcel(transformedData)} >
                            Xuất file Excel
                        </Button>

                        {/* <Button type="primary" icon={<PlusOutlined />} >
                            Thêm mới
                        </Button> */}

                    </Row>

                </Row>
            </Col>
            <Tabs
                defaultActiveKey="1"
                items={[
                    {
                        label: <span>
                            <ContainerOutlined />
                            Tất cả đơn
                            <Tag color="#e83e8c" style={{ marginLeft: 5 }}>{data.length}</Tag>
                        </span>,
                        key: '1',
                        children: <Table className='table' columns={columns} dataSource={data} scroll={{ y: 430 }} pagination={{
                            current: pagination.current,
                            pageSize: pagination.pageSize,
                            total: data.length,
                            onChange: handleChangePagination
                        }} />,
                    },
                    {
                        label: <span>
                            <SolutionOutlined />
                            Đơn hàng chờ xử lý
                            <Tag color="#e83e8c" style={{ marginLeft: 5 }}>{dataPending.length}</Tag>
                        </span>,
                        key: '2',
                        children: <Table className='table' columns={columns} dataSource={dataPending} scroll={{ y: 430 }} pagination={{
                            current: pagination.current,
                            pageSize: pagination.pageSize,
                            total: dataPending.length,
                            onChange: handleChangePagination
                        }} />,
                    },
                    {
                        label:
                            <span>
                                <CheckSquareOutlined />
                                Lịch sử đơn hàng
                                <Tag color="#e83e8c" style={{ marginLeft: 5 }}>{dataHistory.length}</Tag>
                            </span>,
                        key: '3',
                        children: <Table className='table' columns={columns} dataSource={dataHistory} scroll={{ y: 430 }} pagination={{
                            current: pagination.current,
                            pageSize: pagination.pageSize,
                            total: dataHistory.length,
                            onChange: handleChangePagination
                        }} />,
                    },
                    {
                        label: <span>
                            <EditOutlined />
                            Nhận xét
                        </span>,
                        key: '4',
                        disabled: true,
                    },
                ]}
            />


            <DeleteOrder open={openDelete} setOpen={setOpenDelete} item={itemOrder} getAll={getOrder} />

            <UpdateOrder open={openUpdate} setOpen={setOpenUpdate} item={itemOrder} getAll={getOrder} />

            <DetailOrder open={openDetail} setOpen={setOpenDetail} item={itemOrder} />

            <DetailOrderProducts open={openDetailProduct} setOpen={setOpenDetailProduct} item={itemOrder} />

        </>
    )
}
