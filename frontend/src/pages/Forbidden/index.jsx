import { Button, Col, Result } from 'antd';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';

export const Forbidden = () => {
    const navigate = useNavigate()
    const backHome = () => {
        navigate("/")
    }

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)

    // check role
    const MANAGER = user?.user.Role.includes("MANAGER");
    const USER = user?.user.Role.includes("USER");



    useEffect(() => {
        if (MANAGER) {
            toast.warning("Chỉ ADMIN có quyền truy cập !")
        }
        if (USER || !user) {
            toast.warning("Không có quyền truy cập !")
        }
    }, [])

    return (
        <Col style={{ marginTop: '5%' }}>
            <Result
                status="403"
                title="403"
                subTitle="Xin lỗi, Bạn không được phép truy cập trang này."
                extra={<Button type="primary" style={{ background: "#e83e8c" }} onClick={backHome}>Quay về Trang chủ</Button>}
            />
        </Col>
    )
};
