import { CreditCardOutlined, LoadingOutlined, SendOutlined, StarOutlined } from '@ant-design/icons';
import { Button, Col, Grid, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { Autoplay, Scrollbar } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { Swiper, SwiperSlide } from "swiper/react";
import { getAllProduct } from '../../API/apiRequest';
import "../../CSS/Home.css";
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import ProductItem from '../../components/ProductItem';



const { useBreakpoint } = Grid;

export default function Home() {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const screens = useBreakpoint();


    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const productsData = useSelector((state) => state.product.productList?.products)
    // console.log(productsData)

    const accessToken = user?.accessToken


    const getDataProduct = async () => {
        getAllProduct(dispatch, 0, accessToken)
    }

    const imgCoverSilde = [
        {
            titleHealding: 'EVENT PLANING AND MANAGERMENT',
            des: 'We do  event planning management for all different types of events from weddings, birthdays, coopera...',
            btn1: 'BUY NOW',
            btn2: 'INQUIRE',
            price: '$50.00'
        },
        {
            titleHealding: 'BEADS',
            des: 'We have a wide range of beads from hair beads, necklaces, bracelets etc',
            btn1: 'BUY NOW',
            btn2: 'INQUIRE',
            price: '$50.00'
        }
    ]


    useEffect(() => {
        const timer = setTimeout(() => {
            setLoading(false);
        }, 1300);

        return () => clearTimeout(timer);
    }, []);

    useEffect(() => {
        if (user?.accessToken) {
            getDataProduct()
        }
    }, [])

    useEffect(() => {
        setData(productsData)
    }, [productsData])

    return (
        <>
            <Header />
            <Col span={24} style={{ marginBottom: 10 }}>
                <Swiper
                    cssMode={true}
                    scrollbar={{
                        hide: true,
                    }}
                    modules={[Scrollbar, Autoplay]}
                    className="mySwiper"
                    autoplay={{
                        delay: 1500,
                        disableOnInteraction: false,
                    }}

                >
                    {imgCoverSilde.map((item, index) => {
                        return (
                            <SwiperSlide>
                                <Col span={24} className='img_titless' key={index}>
                                    <Row style={{ alignItems: 'center', justifyContent: 'space-between', maxWidth: 1620 }}>
                                        <Col className='img_title_inside'>
                                            <Col>
                                                <h1>{item.titleHealding}</h1>
                                                <p style={{ width: "71%" }}>{item.des}</p>
                                            </Col>
                                            <Row style={{ alignItems: 'center', marginTop: 30 }}>
                                                <Button className='btn_inside' ghost>{item.btn1}</Button>
                                                <Button className='btn_inside2'>{item.btn2}</Button>
                                            </Row>
                                        </Col>
                                        <Col className='sale_inside1'>
                                            <Col style={{ border: '1px solid white', borderRadius: "50%" }}>
                                                <Col style={{ padding: 6 }}>
                                                    <h4>from</h4>
                                                    <h2>{item.price}</h2>
                                                    <p>SHOP NOW</p>
                                                </Col>
                                            </Col>
                                        </Col>
                                    </Row>
                                </Col>
                            </SwiperSlide>
                        );
                    })}
                </Swiper>

                <Col span={24} style={{ textAlign: "center", background: "#f8f8f8", height: "40px", }}>
                    <Row style={{ alignItems: "center", }}>
                        <Col md={8} sm={24} xs={24} >
                            <CreditCardOutlined />
                            <span style={{ marginLeft: 10, fontSize: 17, fontWeight: 'bold' }}> Fast Secure Payments </span>
                        </Col>
                        <Col md={8} sm={24} xs={24} style={{ background: "#e83e8c", color: "white", height: 50, padding: 15 }}>

                            <StarOutlined />
                            <span style={{ marginLeft: 10, fontSize: 17, fontWeight: 'bold' }}>Premium Products</span>

                        </Col>
                        <Col md={8} sm={24} xs={24}>
                            <SendOutlined />
                            <span style={{ marginLeft: 10, fontSize: 17, fontWeight: 'bold' }}>Affordable Delivery</span>
                        </Col>
                    </Row>
                </Col>
                <h2 style={{ textAlign: 'center', marginTop: 60, marginBottom: 20 }}>LATEST PRODUCTS</h2>
                <Col span={24} style={{ margin: "0 auto" }}>
                    <Row
                        style={{
                            justifyContent: "center",
                            textAlign: 'center',
                            alignItems: 'center',
                            overflowX: 'hidden',
                            padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0
                        }}
                    >
                        <Swiper
                            slidesPerView={4}
                            className="mySwiper"
                            autoplay={{
                                delay: 1000,
                                disableOnInteraction: false,
                            }}
                            modules={[Autoplay]}
                            breakpoints={{
                                1724: {
                                    slidesPerView: 4,
                                },
                                768: {
                                    slidesPerView: 3,
                                },
                                500: {
                                    slidesPerView: 2,
                                },
                                300: {
                                    slidesPerView: 1,
                                },
                                100: {
                                    slidesPerView: 1,
                                }
                            }}

                        >
                            {data.products?.map((item) => {
                                return (
                                    <SwiperSlide key={item.id}>
                                        <ProductItem key={item.id} item={item} span={24} />
                                    </SwiperSlide>
                                )
                            })}
                        </Swiper>
                    </Row>
                    <Row style={{ justifyContent: "center", alignItems: 'center', marginTop: 50, padding: 6, gap: 3 }}>
                        <Col className='img_cover'>
                            <Row style={{ justifyContent: 'space-between', maxWidth: 400, margin: '0 auto', marginTop: 30 }}>
                                <Col>
                                    <h2 style={{ color: 'white' }}>HANMADE</h2>
                                    <span style={{ color: 'white' }}>HURRY! 60% OFF!</span> <br />
                                    <Button className='btn_cover'>SHOP NOW</Button>
                                </Col>
                                <Col>
                                    <h4 className='text_cover'>Sale</h4>
                                </Col>
                            </Row>
                        </Col>
                        <Col className='img_cover1'>
                            <Row style={{ justifyContent: 'space-between', maxWidth: 400, margin: '0 auto', marginTop: 30 }}>
                                <Col>
                                    <h2 style={{ color: 'white' }}>HANMADE</h2>
                                    <span style={{ color: 'white' }}>HURRY! 60% OFF!</span> <br />
                                    <Button className='btn_cover'>SHOP NOW</Button>
                                </Col>
                                <Col>
                                    <h4 className='text_cover1'>New</h4>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <h2 style={{ textAlign: 'center', marginTop: 50, marginBottom: 20 }}>BROWSE TOP SELLING PRODUCTS</h2>
                    <Row
                        style={{
                            justifyContent: "center",
                            alignItems: 'center',
                            flexWrap: "wrap",
                            textAlign: 'center', 
                            padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0
                        }}
                    >
                        {/* lấy 8 ảnh gồm những ảnh mới nhất từ dưới lên */}
                        {data?.products?.slice(0, 8).sort((a, b) => b.orderCount - a.orderCount).map((item) => {
                            return (
                                <ProductItem key={item.id} item={item} span={screens.xxl ? 6 : screens.xl ? 8 : screens.md ? 8 : 0} />
                            )
                        })}
                    </Row>

                    <Col className='img_cover2'>
                        <Row style={{ justifyContent: 'space-between', maxWidth: 900, margin: '0 auto', paddingTop: 30, marginTop: 30 }}>
                            <Col>
                                <h2 style={{ color: 'white' }}>BEADING TOOLS</h2>
                                <span style={{ color: 'white' }}>3% DISCOUNT</span> <br />
                                <Button className='btn_cover'>SHOP NOW</Button>
                            </Col>
                            <Col>
                                <h4 className='text_cover1'>NEW</h4>
                            </Col>
                        </Row>
                    </Col>
                </Col>
            </Col>
            <Footer />
        </>
    )
}
