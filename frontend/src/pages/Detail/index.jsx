import { ArrowLeftOutlined, ShoppingCartOutlined, HeartOutlined } from '@ant-design/icons';
import { Breadcrumb, Button, Checkbox, Col, Collapse, Divider, Grid, Image, Input, Row, message } from 'antd';
import Link from 'antd/es/typography/Link';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
import { AddToCart } from '../../API/apiRequest';
import "../../CSS/Detail.css";
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import Money from '../../components/Money';
import { toast } from 'react-toastify';
import { getAllCart } from '../../API/apiRequest';
import { IncreaseCart } from '../../API/apiRequest';

const { useBreakpoint } = Grid;

export default function Detail() {
    const { Panel } = Collapse;
    const { TextArea } = Input;
    const screens = useBreakpoint();
    const idItem = useParams().id
    const dispatch = useDispatch();
    const [quantity, setQuantity] = useState(1);
    const [totla, setTotal] = useState()
    const [valueSize, setValueSize] = useState(["S"]);
    const [noteItem, setNoteItem] = useState("")
    const [isLoading, setIsLoading] = useState(false);

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const productsData = useSelector((state) => state.product.productList?.products)
    const dataCart = useSelector((state) => state.cart.cartList?.allCart);
    // console.log("data", dataCart)

    const itemDetail = productsData.products.filter((item) => {
        if (item._id === idItem) {
            return item
        }
    })

    const stockProducts = itemDetail[0].quantityItem - itemDetail[0].orderCount


    const userId = {
        userId: user?.user._id
    }
    const getCart = async () => {
        await getAllCart(userId, dispatch)
    }

    const plusQuantity = () => {
        setQuantity(quantity + 1)
        if (quantity >= stockProducts) {
            toast.warning("Không đủ số lượng sản phẩm trong kho !")
        }
    }
    const excpetQuantity = () => {
        setQuantity(quantity - 1)
        if (quantity === 1) {
            setQuantity(1)
        }
    }

    const HandleSize = (checkedValues) => {
        setValueSize(checkedValues)
    }


    const handleAddtoCart = async () => {
        if (!user) {
            toast.warning("Đăng nhập để thêm vào giỏ hàng")
        }
        if (isLoading) {
            return; // Nếu đang tải lên, không thực hiện hành động tiếp theo
        }

        setIsLoading(true);

        const existingItem = dataCart.find(item => item.product === itemDetail[0].title);
        if (existingItem) {
            if (valueSize.length === 0) {
                message.warning("Vui lòng chọn size sản phẩm !")
            } else {
                // Gọi API tăng số lượng
                const quantityToAdd = quantity;
                await IncreaseCart(user?.user._id, existingItem._id, quantityToAdd, dispatch);
                message.warning("Sản phẩm đã được tăng trong giỏ hàng !")
            }

        } else {
            // Gọi API thêm sản phẩm mới
            const newItem = {
                userId: user?.user._id,
                product: itemDetail[0].title,
                img: itemDetail[0].img,
                quantity: quantity,
                notes: noteItem,
                size: valueSize,
                price: itemDetail[0].price,
                description: itemDetail[0].description
            };
            if (valueSize.length === 0) {
                message.warning("Vui lòng chọn size sản phẩm !")
            }
            if (quantity > stockProducts) {
                toast.warning("Không đủ số lượng sản phẩm trong kho !")
            } else {
                await AddToCart(newItem, dispatch);
            }
        }

        // Cập nhật lại giỏ hàng sau khi thêm sản phẩm
        await getCart();

        setTimeout(() => {
            setIsLoading(false);
        }, 1);
    };


    return (
        <>
            <Header />
            <Col style={{overflowX:'hidden'}}>
                <Col span={24} className='header_cate'>
                    <h2>WEDDING & PARTY ACCESSORIES</h2>
                    <Breadcrumb
                        routes={[
                            { path: '/', breadcrumbName: "Home" },
                            { path: '/categories', breadcrumbName: 'sản phẩm' }]}
                        separator="/"
                        style={{
                            fontSize: 15,
                            fontWeight: 'bold'
                        }}
                    />
                </Col>
                <Col className='back' style={{ marginLeft: screens.xs ? 0 : '24%' , marginTop: 30 }}>
                    <Link href='/categories' style={{ color: "#414141", }}>
                        <ArrowLeftOutlined style={{ paddingRight: 5 }} />
                        <b>Back to Categories</b>
                    </Link>
                </Col>
                <Row className='wrapper_content' style={{ padding: (screens.xxl) ? '0 460px' : (screens.xl) ? '0 200px' : 0, gap:20 }}>
                    <Col xxl={10} xl={8} md={12} sm={24} className='detail_img' style={{textAlign: screens.sm ? 'center' : 'unset'}}>
                        <Image src={itemDetail[0].img} width={520} height={550} />
                        {screens.xxl ? <Row className='child_cover'>
                            <Image src={itemDetail[0].img} width={130} />
                            <Image className='img_child' src={itemDetail[0].img} width={130} />
                            <Image className='img_child2' src={itemDetail[0].img} width={130} />
                        </Row> : ''}
                        
                    </Col>
                    <Col xxl={10} xl={8} md={8} sm={24} className='detail_info' style={{ padding: screens.xs ? 80 : screens.md ? 0 : screens.xl ? 0 : 0 }}>
                        <h2>{itemDetail[0].title}</h2>
                        <h1 style={{ color: "#f51167" }}><Money value={itemDetail[0].price} /></h1>
                        <h4>
                            Availability:
                            {stockProducts > 0 ? <b style={{ color: '#f51167' }}> In Stock</b> : <b style={{ color: 'red' }}> Out Stock</b>}

                        </h4>
                        {stockProducts > 0 ? <h4>
                            Stock:
                            <b style={{ color: '#f51167' }}> {stockProducts} products left</b>
                        </h4> : ''}

                        <Col className='quantity_cart'>
                            <Row style={{ marginBottom: 15 }}>
                                <h3 >SIZE</h3>
                                <Col style={{ marginLeft: 40, }}>
                                    <Checkbox.Group style={{ marginTop: 5 }} value={valueSize} onChange={HandleSize}>
                                        <Checkbox value="S"><b>S</b></Checkbox>
                                        <Checkbox value="M"><b>M</b></Checkbox>
                                        <Checkbox value="L"><b>L</b></Checkbox>
                                    </Checkbox.Group>
                                </Col>
                            </Row>
                            <Row style={{ marginTop: 20, alignItems: "center" }}>
                                <h3>QUANTITY</h3>
                                <Col className='quantity' style={{ marginLeft: 30, userSelect: "none" }}>
                                    <span className='quantity_except' onClick={excpetQuantity}>-</span>
                                    <input type='text' className='inputs' value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                                    <span className='quantity_plus' onClick={plusQuantity} >+</span>
                                </Col>
                            </Row>
                            <Row style={{ marginTop: 30 }}>
                                <h3>NOTE</h3>
                                <Col span={19} style={{ marginLeft: 20 }}>
                                    <TextArea value={noteItem} onChange={(e) => setNoteItem(e.target.value)} rows={4} placeholder="Note product . . ." maxLength={50} showCount />
                                </Col>
                            </Row>
                            <Col style={{ marginTop: 30 }}>
                                <Button className='btn_add' onClick={handleAddtoCart}>
                                    <b>ADD TO CART</b>
                                </Button>
                            </Col>
                            <Divider />
                            <Col>
                                <Collapse ghost >
                                    <Panel header={<b style={{ color: '#8c8c8c' }}>DESCRIPTION</b>} >
                                        <b style={{ color: '#8c8c8c' }}>{itemDetail[0].description}</b>
                                    </Panel>
                                </Collapse>
                                <Divider className="custom-divider" />
                                <Collapse ghost defaultActiveKey={['0']}>
                                    <Panel header={<b style={{ color: '#8c8c8c' }}>SHIPPING & RETURNS</b>} >
                                        <b style={{ color: '#8c8c8c' }}>{itemDetail[0].description}</b>
                                    </Panel>
                                </Collapse>
                            </Col>
                            <Divider />
                        </Col>
                    </Col>
                </Row>
                <Col>
                    <h1 style={{ textAlign: 'center', marginBottom: 50 }}>RELATED PRODUCTS</h1>
                    <Row style={{
                        justifyContent: "center",
                        textAlign: 'center',
                        alignItems: 'center',
                        overflowX: 'hidden',
                        marginBottom: 50,
                        padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0
                    }}>
                        <Swiper
                            slidesPerView={4}
                            spaceBetween={10}
                            className="mySwiper"
                            autoplay={{
                                delay: 2000,
                                disableOnInteraction: false,
                            }}
                            modules={[Autoplay]}
                            breakpoints={{
                                1724: {
                                    slidesPerView: 4,
                                },
                                768: {
                                    slidesPerView: 3,
                                },
                                500: {
                                    slidesPerView: 2,
                                },
                                300: {
                                    slidesPerView: 1,
                                },
                                100: {
                                    slidesPerView: 1,
                                }
                            }}
                        >
                            {productsData.products.map((item) => {
                                return (
                                    <SwiperSlide key={item.id}>
                                        <Col className='slide_img' onClick={() => handleDetail(item._id)}>
                                            <img src={item.img} style={{ width: "255px" }} />
                                            <Row style={{ justifyContent: 'space-between', marginTop: 10 }}>
                                                <Link>
                                                    <p style={{ fontSize: 13, color: "black", }}>{item.title}</p>
                                                </Link>
                                                <Col>
                                                    <h3><Money value={item.price} /></h3>
                                                </Col>
                                                <div className='overlayss'></div>
                                                <Button className='add_to_carts'> <ShoppingCartOutlined />Add to cart</Button>
                                                <Button className='favorite_buttons'><HeartOutlined /></Button>
                                            </Row>
                                        </Col>
                                    </SwiperSlide>
                                )
                            })}
                        </Swiper>
                    </Row>
                </Col>
            </Col >
            <Footer />
        </>
    )
}
