import { CloseCircleFilled, CreditCardOutlined, ShoppingCartOutlined, CloseOutlined, CheckOutlined, HeartOutlined } from '@ant-design/icons';
import { Checkbox, Col, Divider, Form, Grid, Image, Input, Row, Select, Space, Steps, Table, Tag, Tooltip, message } from 'antd';
import Radio, { Button } from 'antd/es/radio';
import Link from 'antd/es/typography/Link';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
import { DecreaseCart, DeleteCart, IncreaseCart, getAllCart } from '../../API/apiRequest';
import "../../CSS/Cart.css";
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import Money from '../../components/Money';
import axios from 'axios';
import { useNavigate } from 'react-router';
import { Paypal } from "../../components/Paypal";


const { useBreakpoint } = Grid;

export default function Cart() {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const CheckboxGroup = Checkbox.Group;
    const screens = useBreakpoint();
    const { Search } = Input;
    const { TextArea } = Input;
    const { Step } = Steps;
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const [data, setData] = useState([])
    const [totalCart, setTotalCart] = useState(0);
    const [totlaQuantity, setTotalQuantity] = useState(0);
    const [quantityItem, setQuantityItem] = useState(1);
    const [priceItem, setPriceItem] = useState()
    const [showInfo, setShowInfo] = useState(false)
    const [currentOrder, setCurrentOder] = useState(1)

    // Đơn hàng
    const [itemName, setItemName] = useState([])
    const [customerName, setCustomerName] = useState("");
    const [customerPhone, setCustomePhone] = useState();
    const [customerAddress, setCustomerAddress] = useState("");
    const [orderPay, setOrderPay] = useState(1);
    const [orderDes, setOrderDes] = useState("");
    const [orderStatus, setOrderStatus] = useState(["order"])
    const [itemIds, setItemIds] = useState([]);
    const [productId, setProductId] = useState([])



    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const dataCart = useSelector((state) => state.cart.cartList?.allCart);
    const productsData = useSelector((state) => state.product.productList?.products)
    // console.log(dataCart);

    // Tăng 
    const handleIncrease = async (value) => {
        await IncreaseCart(user?.user._id, value._id, 1, dispatch);
        setQuantityItem(quantityItem + 1);
        setPriceItem(priceItem + value.quantity);

        // Cập nhật lại dữ liệu bảng
        const updatedData = data.map(item => {
            if (item._id === value._id) {
                return {
                    ...item,
                    quantity: item.quantity + 1,
                    subtotal: item.subtotal + item.price
                };
            }
            return item;
        });
        setData(updatedData);

        // Tính toán lại totalCart & quantity cart
        const subtotalArray = updatedData.map(item => item.subtotal);
        const total = subtotalArray.reduce((accumulator, currentValue) => accumulator + currentValue);
        const quantityTotalArray = dataCart.map(item => item.quantity);
        const totalQuantityCart = quantityTotalArray.reduce((accumulator, currentValue) => accumulator + currentValue);

        // console.log("Tổng tiền:", subtotalArray)
        // console.log("Tổng số lượng:", quantityTotalArray);

        setTotalQuantity(totalQuantityCart)
        setTotalCart(total);
        getCart()
    };
    console.log(data);

    // Giảm 
    const handleDecrease = async (value) => {
        await DecreaseCart(user?.user._id, value._id, 1, dispatch);
        setQuantityItem(quantityItem - 1);
        setPriceItem(priceItem - value.quantity);
        // Cập nhật lại dữ liệu bảng
        const updatedData = data.map(item => {
            if (item._id === value._id) {
                return {
                    ...item,
                    quantity: item.quantity - 1,
                    subtotal: item.subtotal - item.price
                };
            }
            return item;
        });
        setData(updatedData);

        // Tính toán lại totalCart & quantity cart
        const subtotalArray = updatedData.map(item => item.subtotal);
        const total = subtotalArray.reduce((accumulator, currentValue) => accumulator + currentValue);
        const quantityTotalArray = dataCart.map(item => item.quantity);
        const totalQuantityCart = quantityTotalArray.reduce((accumulator, currentValue) => accumulator + currentValue);
        setTotalQuantity(totalQuantityCart)
        setTotalCart(total);
        getCart()
    };

    const handleDeleteItemCart = async (value) => {
        await DeleteCart(user?.user._id, value._id, dispatch)
        getCart()
    }

    const userId = {
        userId: user?.user._id
    }
    // console.log(userId)

    const getCart = async () => {
        await getAllCart(userId, dispatch)
    }

    const increaseOrderCount = async (itemID, itemQuantityOrder) => {
        try {
            await axios.post(`${baseURL}product/increaseOrderCount/${itemID}`, {
                quantityItemOrder: itemQuantityOrder
            })
        } catch (e) {
            console.log("Lỗi tăng: ", e)
        }
    }

    // ĐẶT HÀNG
    const handleOrder = async () => {
        const newOrder = {
            userId: user?.user._id,
            order_products: [
                {
                    item: itemName,
                    quantity: totlaQuantity,
                    price: totalCart
                }
            ],
            customer_name: customerName,
            customer_phone: customerPhone,
            customer_address: customerAddress,
            order_pay: orderPay,
            order_description: orderDes,
            order_status: orderStatus
        }
        console.log("Đơn hàng: ", newOrder)
        try {
            itemIds.forEach((item) => DeleteCart(user?.user._id, item, dispatch))
            await axios.post(`${baseURL}order/addNewOrder`, newOrder)

            // Tăng orderCount lên 1
            productId.forEach((item) => increaseOrderCount(item, totlaQuantity))

            message.success("Đặt hàng thành công !")
            setData([]);
            setCurrentOder(currentOrder + 2)
            setShowInfo(false);
            setTotalCart(0);
            getCart()

        } catch (e) {
            console.log({ Err: e })
        }
    }

    // Lọc ra các subtotal của từng dataCart
    useEffect(() => {
        if (dataCart.length > 0) {
            const subtotalArray = dataCart.map(item => item.subtotal);

            const quantityTotalArray = dataCart.map(item => item.quantity);
            const totalQuantityCart = quantityTotalArray.reduce((accumulator, currentValue) => accumulator + currentValue);
            const total = subtotalArray.reduce((accumulator, currentValue) => accumulator + currentValue);
            setTotalCart(total);
            setTotalQuantity(totalQuantityCart)

            // lấy id của sản phẩm
            const itemId = dataCart.map(item => item._id);
            setItemIds(itemId)
            // const titleItemCart = dataCart.map((item) => item.product)
            const productIds = productsData.products.filter((item) => dataCart.map((item) => item.product).includes(item.title)).map((item) => item._id);
            setProductId(productIds)


        } else {
            setTotalCart(0);
        }
    }, [dataCart]);

    useEffect(() => {
        if (userId) {
            getCart()
        }
    }, [])

    useEffect(() => {
        setData(dataCart)
    }, [dataCart])

    useEffect(() => {
        // dữ liệu sản phẩm 
        const itemNames = dataCart.map((item) => ({ name: item.product, size: item.size, quantity: item.quantity, avatar: item.img, note: item.notes }));
        setItemName(itemNames);

    }, [dataCart])

    const columns = [
        {
            title: <><Checkbox /></>,
            key: 'check',
            render: (_, record) => (
                <>
                    <Checkbox />
                </>
            ),
        },
        {
            title: 'Product',
            dataIndex: 'product',
            key: 'product',
            width: 400,
            render: (_, record) => (
                <Row>
                    <Image
                        width={50}
                        src={record.img}
                    />
                    <Col style={{ marginLeft: 15 }}>
                        <b>{record.product}</b>
                        <br />
                        <p className='des_cart'>{record.description}</p>
                    </Col>
                </Row>
            ),
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            key: 'quantity',
            width: 250,
            render: (_, record) => (
                <Row style={{ justifyContent: 'space-between', alignItems: "center", maxWidth: "150px" }}>
                    <Col className='quantity'>
                        <span className='quantity_except' onClick={() => handleDecrease(record)}>-</span>
                        <input type='text' className='inputs' value={record.quantity} />
                        <span className='quantity_plus' onClick={() => handleIncrease(record)}>+</span>
                    </Col>
                    <Col><CheckOutlined style={{ color: 'green' }} /></Col>
                </Row>

            )
        },
        {
            title: 'Size',
            dataIndex: 'size',
            key: 'size',
            render: (value) => <b>{value.toLocaleString('')}</b>
        },
        {
            title: 'Price',
            dataIndex: 'subtotal',
            key: 'subtotal',
            render: (value) => <Tag color="purple"><Money value={value} /></Tag>
        },
        {
            title: '',
            key: 'action',
            render: (_, record) => (
                <Space size="middle">
                    <Tooltip placement="top" title="Xóa">
                        <CloseCircleFilled style={{ color: "red" }} onClick={() => handleDeleteItemCart(record)} />
                    </Tooltip>
                </Space>
            ),
        },
    ];
    return (
        <>
            <Header />
            {dataCart.length > 0 || currentOrder === 3 ? <Col span={16} style={{ marginTop: 30, marginLeft: "15%" }}>
                <Steps current={currentOrder} >
                    <Step title="Đặt hàng" />
                    <Step title="Xác nhận đơn hàng" />
                    <Step title="Thành công" />
                </Steps>
            </Col > : ''}


            {currentOrder === 3 ? <Col>
                <h1 style={{ margin: "139px 0", textAlign: 'center' }}>
                    Cảm ơn bạn đã tin dùng sản phẩm của chúng tôi ! <br />
                    <Button type="primary" shape="round" size='large'
                        style={{ backgroundColor: '#f51167', color: "white" }}
                        onClick={() => navigate('/order-history')}>
                        Bạn có thể xem chi tiết đơn hàng tại đây
                    </Button>
                </h1>
            </Col> :
                <Col>
                    <Row style={{ marginTop: 10, justifyContent: 'center', gap: 40 }}>
                        <Col xl={12} sm={24} className='wrapp_cart' style={{ marginBottom: "7%" }}>
                            <Col className='cart_table'>
                                <h1>✨ Your cart ✨ </h1>
                                <Col className='table'>
                                    <Table columns={columns} dataSource={data} scroll={{ x: 728 }} />
                                </Col>
                            </Col>
                            <Row className='total' >
                                <h1>Total</h1>
                                <h2><Money value={totalCart} /></h2>
                            </Row>
                        </Col>
                        <Col xl={6} sm={24} className='cart_right'>
                            <Col>
                                <Search
                                    className='footer_subcirbe'
                                    placeholder="Enter promo-code"
                                    allowClear
                                    enterButton="SUBMIT"
                                    size="large"

                                />
                            </Col>
                            <Col className='shipping_options'>
                                <h3>SHIPPING OPTIONS</h3>
                                <Row className="select_options">
                                    <b>Select Zone</b>
                                    <Select
                                        defaultValue="Select"
                                        style={{
                                            width: 120,
                                        }}
                                        options={[
                                            {
                                                value: 'select',
                                                label: 'select',
                                            },
                                            {
                                                value: 'kamafia',
                                                label: 'kamafia',
                                            },
                                            {
                                                value: 'Eatsled',
                                                label: 'Eatsled',
                                            },
                                            {
                                                value: 'Westgate',
                                                label: 'Westgate',
                                            },
                                        ]}
                                    />
                                </Row>
                                <Button className='btn_caculate'>
                                    <b>Caculate</b>
                                </Button>
                            </Col>
                            <Button className='btn_checkOut' onClick={() => setShowInfo(true)}>
                                <Link style={{ color: 'white' }}><b>PROCEED TO CHECK OUT</b></Link>
                            </Button><br />
                            <Button className='btn_continue'>
                                <Link href='/categories' style={{ color: 'white' }}><b>CONTINUE SHOPPING</b></Link>
                            </Button>
                        </Col>
                    </Row>
                    {showInfo && user && dataCart.length > 0 ? <Col className='infor_user' style={{ margin: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : '0 60px' }}>
                        <Col>
                            <Col className='cart_table'>
                                <Col style={{ display: 'flex', justifyContent: 'flex-end', cursor: 'pointer' }}>
                                    <CloseOutlined onClick={() => setShowInfo(false)} />
                                </Col>
                                <h1 style={{ textAlign: 'center' }}>Thông tin giao hàng <ShoppingCartOutlined /></h1>
                                <Col className='infor' span={24} style={{ marginTop: 35 }}>
                                    <Row gutter={24}>
                                        <Col xl={12} sm={24}>
                                            <Form.Item
                                                name="name"
                                                label="Tên người nhận"
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Xin vui lòng nhập tên',
                                                    },
                                                ]}
                                            >
                                                <Input placeholder="Tên người nhận" value={customerName} onChange={(e) => setCustomerName(e.target.value)} />
                                            </Form.Item></Col>
                                        <Col xl={12} sm={24}>
                                            <Form.Item
                                                name="phone"
                                                label="Số điện thoại"
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Xin vui lòng nhập số điện thoại',
                                                    },
                                                ]}
                                            >
                                                <Input type='number' placeholder="SĐT" value={customerPhone} onChange={(e) => setCustomePhone(e.target.value)} />
                                            </Form.Item>
                                        </Col>

                                    </Row>
                                    <Col >
                                        <Form.Item
                                            name="address"
                                            label="Địa chỉ giao hàng"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Xin vui lòng nhập địa chỉ',
                                                },
                                            ]}
                                        >
                                            <TextArea placeholder="Địa chỉ . . ." value={customerAddress} onChange={(e) => setCustomerAddress(e.target.value)} rows={3} showCount maxLength={150} />
                                        </Form.Item>
                                    </Col>
                                    <Col style={{ marginTop: 30 }}>
                                        <Form.Item
                                            name="name"
                                            label="Ghi chú"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Xin vui lòng nhập tên',
                                                },
                                            ]}
                                        >
                                            <TextArea value={orderDes} onChange={(e) => setOrderDes(e.target.value)} rows={4} type='text' placeholder="Thêm ghi chú giao hàng  . . ." showCount maxLength={150} />
                                        </Form.Item>
                                    </Col>
                                </Col>
                                <br />
                                <Col>
                                    <h1 style={{ textAlign: 'center', }}>Phương thức thanh toán <CreditCardOutlined /></h1>
                                    <Radio.Group value={orderPay} onChange={(e) => setOrderPay(e.target.value)}>
                                        <Radio value={1}><b>💸Tiền mặt</b></Radio>
                                        <Divider style={{ width: 1060 }} />
                                        <Radio value={2}><b> 💳Thẻ ngân hàng</b></Radio>
                                    </Radio.Group>
                                    {orderPay === 2 && <Col style={{ marginTop: "24px", marginLeft: "14%" }}>
                                        <Paypal handleOrder={handleOrder} totalPrice={totalCart} cartItem={data} />
                                    </Col>}

                                    <Col style={{ marginTop: 50 }}>
                                        <Checkbox ><b>  Đồng ý với các{" "}
                                            <span style={{ color: "#f51167" }}>
                                                điều khoản và điều kiện
                                            </span>{" "}
                                            mua hàng của <span style={{ color: "#f51167" }}>ILUBOOKS</span></b></Checkbox>
                                    </Col>
                                </Col>
                            </Col>
                            <Row className='total'style={{gap:10}} >
                                <Col xl={1} sm={24} xs={24}>
                                    <h2>Total</h2>
                                    <h3><Money value={totalCart} /></h3>
                                </Col>
                                <Col xl={2} sm={24} xs={24}>
                                    <h2>Quantity</h2>
                                    <h2 style={{ textAlign: screens.xl ? 'center' : 'unset' }}>{totlaQuantity}</h2>
                                </Col>
                                <Col xl={4} sm={24} xs={24}>
                                    <Button className='btn_checkout' style={{ borderRadius: 34 }} onClick={handleOrder}>Đặt hàng</Button>
                                </Col>

                            </Row>
                        </Col>
                    </Col>
                        : ''}

                </Col >
            }
            <Col>
                <h1 style={{ textAlign: 'center', margin: '5% 0 2% 0' }}>MIGHT ALSO LIKE</h1>
                <Row style={{
                    justifyContent: "center",
                    textAlign: 'center',
                    alignItems: 'center',
                    overflowX: 'hidden',
                    marginBottom: 50,
                    padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0
                }}>
                    <Swiper
                        slidesPerView={4}
                        spaceBetween={10}
                        className="mySwiper"
                        autoplay={{
                            delay: 2000,
                            disableOnInteraction: false,
                        }}
                        modules={[Autoplay]}
                        breakpoints={{
                            1724: {
                                slidesPerView: 4,
                            },
                            768: {
                                slidesPerView: 3,
                            },
                            500: {
                                slidesPerView: 2,
                            },
                            300: {
                                slidesPerView: 1,
                            },
                            100: {
                                slidesPerView: 1,
                            }
                        }}
                    >
                        {productsData.products.map((item) => {
                            return (
                                <SwiperSlide key={item.id}>
                                    <Col className='slide_img' onClick={() => handleDetail(item._id)}>
                                        <img src={item.img} style={{ width: "255px" }} />
                                        <Row style={{ justifyContent: 'space-between', marginTop: 10 }}>
                                            <Link>
                                                <p style={{ fontSize: 13, color: "black", }}>{item.title}</p>
                                            </Link>
                                            <Col>
                                                <h3><Money value={item.price} /></h3>
                                            </Col>
                                            <div className='overlayss'></div>
                                            <Button className='add_to_carts'> <ShoppingCartOutlined />Add to cart</Button>
                                            <Button className='favorite_buttons'><HeartOutlined /></Button>
                                        </Row>
                                    </Col>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </Row>
            </Col>
            <Footer />
        </>
    )
}
