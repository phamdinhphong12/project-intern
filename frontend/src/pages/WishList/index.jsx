import { HeartOutlined, HeartFilled, ShoppingCartOutlined } from '@ant-design/icons';
import { Avatar, Breadcrumb, Col, Divider, List, Row, Button, Grid, } from 'antd';
import React, { useEffect, useState } from 'react';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import "../../CSS/wishList.css"
import Link from 'antd/es/typography/Link';
import { Autoplay } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
import { useDispatch, useSelector } from 'react-redux';
import Money from '../../components/Money';
import { getAllFavorite, deleteFavorite } from '../../API/apiRequest';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router';
import axios from 'axios';

export default function WishList() {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const { useBreakpoint } = Grid;
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const screens = useBreakpoint();

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const productsData = useSelector((state) => state.product.productList?.products)
    const favouriteData = useSelector((state) => state.favourite.favouriteList?.allFavourite)
    // console.log(favouriteData);

    const userId = {
        userId: user?.user._id
    }
    const [dataAll, setDataAll] = useState([])


    const getAllFavorites = () => {
        getAllFavorite(userId, dispatch)
    }

    const handleClick = (item) => {
        navigate(`/detail/${item.productId}`)
        // console.log(item)
    }

    const handleRemove = async (item) => {
        await deleteFavorite(user?.user._id, item.productId, dispatch)
        await axios.put(`${baseURL}user/update-wishList/${user?.user._id}`, {
            productId: item.productId
        });
        getAllFavorites()
    }

    useEffect(() => {
        if (user) {
            getAllFavorites()
        } else {
            toast.warning("Bạn cần đăng nhập mới xem được !!!")
        }
    }, [])

    // check favouriteData có phải là mảng không trước khi sử dụng toán tử spread
    useEffect(() => {
        if (Array.isArray(favouriteData)) {
            setDataAll([...favouriteData]);
        }
    }, [favouriteData]);


    return (
        <>
            <Header />
            <Col span={24} className='header_history' >
                <h2>WISHLIST PAGE</h2>
                <Breadcrumb
                    routes={[
                        { path: '/', breadcrumbName: "Home" },
                        { path: '/categories', breadcrumbName: 'Wish List' }]}
                    separator="/"
                    style={{
                        fontSize: 15,
                        fontWeight: 'bold'
                    }}
                />
            </Col>
            <Row style={{ justifyContent: "center", gap: 10, padding:'0 20px'}}>
                <Col xl={9} md={9} sm={24} xs={24} className='wrapp_wishList' style={{ marginBottom: "7%" }}>
                        <Col className='cart_table_history'>
                            <h1 style={{ textAlign: 'center' }}>✨ Your Wish List ✨ </h1>
                            <Divider />

                            <Col className='col_wrapp'>
                                <List
                                    itemLayout="horizontal"
                                    dataSource={dataAll}
                                    renderItem={(item, index) => (
                                        <List.Item key={index} className='list_item' >
                                            <List.Item.Meta onClick={() => handleClick(item)}
                                                avatar={<Avatar size={60} src={item.img} />}
                                                title={<b >{item.product}</b>}
                                                description={item.description}

                                            />
                                            <p style={{ color: '#e83e8c', cursor: "pointer" }} onClick={() => handleRemove(item)}> <HeartFilled /></p>
                                        </List.Item>
                                    )}
                                />
                            </Col>
                        </Col>
                </Col>
                <Col xl={3} md={6} sm={24} xs={24} className='wish_right'>
                    <Button className='btn_checkOut'>
                        <Link style={{ color: 'white' }}><b>PROCEED TO CHECK OUT</b></Link>
                    </Button><br />
                    <Button className='btn_continue'>
                        <Link href='/categories' style={{ color: 'white' }}><b>CONTINUE SHOPPING</b></Link>
                    </Button>
                </Col>
            </Row>
            <Col>
                <h1 style={{ textAlign: 'center', marginBottom: 50 }}>RELATED PRODUCTS</h1>
                <Row style={{
                    justifyContent: "center",
                    textAlign: 'center',
                    alignItems: 'center',
                    overflowX: 'hidden',
                    padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0,
                    marginBottom: 50
                }}>
                    <Swiper
                        slidesPerView={4}
                        spaceBetween={10}
                        className="mySwiper"
                        autoplay={{
                            delay: 2000,
                            disableOnInteraction: false,
                        }}
                        modules={[Autoplay]}
                        breakpoints={{
                            1724: {
                                slidesPerView: 4,
                            },
                            768: {
                                slidesPerView: 3,
                            },
                            500: {
                                slidesPerView: 2,
                            },
                            300: {
                                slidesPerView: 1,
                            },
                            100: {
                                slidesPerView: 1,
                            }
                        }}
                    >
                        {productsData.products.map((item) => {
                            return (
                                <SwiperSlide key={item.id}>
                                    <Col className='slide_img' onClick={() => handleDetail(item._id)}>
                                        <img src={item.img} style={{ width: "255px" }} />
                                        <Row style={{ justifyContent: 'space-between', marginTop: 10 }}>
                                            <Link>
                                                <p style={{ fontSize: 13, color: "black", }}>{item.title}</p>
                                            </Link>
                                            <Col>
                                                <h3><Money value={item.price} /></h3>
                                            </Col>
                                            <div className='overlayss'></div>
                                            <Button className='add_to_carts'> <ShoppingCartOutlined />Add to cart</Button>
                                            <Button className='favorite_buttons'><HeartOutlined /></Button>
                                        </Row>
                                    </Col>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </Row>
            </Col>
            <Footer />
        </>
    )
}
