import React, { useState } from 'react'
import { Button, Col, Divider, Input, Row } from 'antd'
import { MailOutlined, KeyOutlined, UserOutlined } from '@ant-design/icons';
import "../../CSS/Login.css"
import "../../CSS/Register.css"
import Link from 'antd/es/typography/Link';
import ReCAPTCHA from 'react-google-recaptcha';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { RegisterUser } from '../../API/apiRequest';
import Header from '../../components/Header';
import Footer from '../../components/Footer';


export default function Register() {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [captchaValue, setCaptchaValue] = useState('');
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirm, setConfirm] = useState("")

    const handleCaptchaChange = (value) => {
        setCaptchaValue(value);
    };

    const handleRegister = () => {
        const newUser = {
            username: username,
            email: email,
            password: password,
            confirm: confirm
        }
        if (captchaValue && password === confirm) {
            RegisterUser(newUser, dispatch, navigate)
        }
    }

    return (
        <>
            <Header />
            <Col className='container' span={24}>
                <Row className='wrapp_form_register' >
                    <Col className='img_loginForm' xs={0} md={0} lg={0} xl={6}>
                        <img className='img_register' src='https://i.pinimg.com/564x/90/a2/1d/90a21d03c76b1bf220e339252fb664c9.jpg' />
                    </Col>
                    <Col className='form_register'>
                        <h2 style={{ textAlign: "center" }}>Create an account</h2>
                        <form className='form_login' onSubmit={handleRegister}>
                            <Input className='name' type='text' value={username} onChange={(e) => setUsername(e.target.value)} size="large" placeholder="Name" prefix={<UserOutlined className="custom-mail-icon" />} />
                            <Input className='email' type='text' value={email} onChange={(e) => setEmail(e.target.value)} size="large" placeholder="Email Address" prefix={<MailOutlined className="custom-mail-icon" />} />
                            <Input className='password' type="password" value={password} onChange={(e) => setPassword(e.target.value)} size="large" placeholder="Password" prefix={<KeyOutlined className="custom-mail-icon" />} />
                            <Input className='email' type="password" value={confirm} onChange={(e) => setConfirm(e.target.value)} size="large" placeholder="Confirm password" prefix={<KeyOutlined className="custom-mail-icon" />} />
                            <br />

                            <ReCAPTCHA
                                sitekey="6LdVGU8mAAAAAH3z4_1rqmpCe0lriyklyBiZpL-Z"
                                onChange={handleCaptchaChange}
                            />

                            <Button className='btn_register' onClick={handleRegister}>Register</Button>
                        </form>
                        <p className='orLogin' >or login with</p>
                        <Row className='warpp_orther'>
                            <Button className='btn_facebook'>Facebook</Button>
                            <Button className='btn_email'>Google</Button>
                        </Row>
                        <Divider />
                        <Col className='bottom_text'>
                            <p> Already have account ? <Link href='/login'>Login here</Link></p>
                        </Col>
                    </Col>
                </Row >
            </Col >
            <Footer />
        </>
    )
}
