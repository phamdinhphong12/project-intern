import { message } from "antd";
import axios from "axios";
import { toast } from 'react-toastify';
import { addToCartFailed, addToCartSuccess, addTocartStart } from "../redux/actionCartSlice";
import { logOutFailed, logOutStart, logOutSuccess, loginFailed, loginStart, loginSuccess, registerFailed, registerStart, registerSuccess } from "../redux/authSlice";
import { getCartFaild, getCartStart, getCartSuccess } from "../redux/cartSlice";
import { decreaseItemFaild, decreaseItemStart, decreaseItemSuccess } from "../redux/decreaseCartSlice";
import { deleteItemFaild, deleteItemStart, deleteItemSuccess } from "../redux/deleteCartSlice";
import { getFavouriteFaild, getFavouriteStart, getFavouriteSuccess } from "../redux/favouriteSlice";
import { increaseItemFailed, increaseItemStart, increaseItemSuccess } from "../redux/increaseCartSlice";
import { getOrderFalid, getOrderStart, getOrderSuccess } from "../redux/orderSlice";
import { getProductFailed, getProductStart, getProductSuccess } from "../redux/productSlice";
import { deleteWishFaild, deleteWishStart, deleteWishSuccess } from "../redux/removeWishListSlice";
import { getRoleFaild, getRoleStart, getRoleSuccess } from "../redux/roleSlice";
import { getUserFaild, getUserStart, getUserSuccess } from "../redux/userSlice";


const baseURL = import.meta.env.VITE_API_PRODUCTS;


// REGISTER
export const RegisterUser = async (newUser, dispatch, navigate) => {
    dispatch(registerStart());
    try {
        const res = await axios.post(`${baseURL}auth/register`, newUser);
        dispatch(registerSuccess(res.data));
        navigate("/login");
        toast.success("Đăng ký tài khoản thành công")

    } catch (e) {
        dispatch(registerFailed());
        toast.error("Đăng ký không thành công !")
    }
}

// LOGIN 
export const LoginUser = async (user, dispatch, navigate) => {
    dispatch(loginStart())
    try {
        const res = await axios.post(`${baseURL}auth/login`, user);
        dispatch(loginSuccess(res.data));
        navigate("/");
        toast.success("Đăng nhập thành công !")

    } catch (e) {
        dispatch(loginFailed());
        toast.error("Đăng nhập không thành công")
    }
}

// LOGOUT 
export const LogoutUser = async (id, dispatch, navigate, accessToken) => {
    dispatch(logOutStart());
    try {
        await axios.post(`${baseURL}auth/logout`, id, {
            headers: { token: `Bearer ${accessToken}` }
        })
        dispatch(logOutSuccess());
        navigate("/login")
        toast.success("Đăng xuất thành công")

    } catch (e) {
        dispatch(logOutFailed());
        toast.error("Đăng xuất không thành công")
    }
}


// GET ALL PRODUCTS
export const getAllProduct = async (dispatch, page, accessToken) => {
    dispatch(getProductStart());
    try {
        let url = `${baseURL}product/getAll`;

        if (page) {
            url += `?page=${page}`;
        }

        const res = await axios.get(url, {
            headers: { token: `Bearer ${accessToken}` },
        });

        dispatch(getProductSuccess(res.data));
    } catch (e) {
        dispatch(getProductFailed());
        toast.error("Thất bại!");
    }
};



// GET ALL CART
export const getAllCart = async (userId, dispatch) => {
    dispatch(getCartStart());
    try {
        const res = await axios.get(`${baseURL}cart/getAllCart`, {
            params: userId
        });
        dispatch(getCartSuccess(res.data))

    } catch (e) {
        dispatch(getCartFaild())
        console.log({ Err: e })
    }
}

// ADD TO CART
export const AddToCart = async (newItem, dispatch) => {
    dispatch(addTocartStart());
    try {
        const res = await axios.post(`${baseURL}cart/addItemCart`, newItem)
        dispatch(addToCartSuccess(res.data));
        // toast.success("Thêm vào giỏ hàng thành công !")
        message.success("Thêm vào giỏ hàng thành công")

    } catch (e) {
        dispatch(addToCartFailed());
        // toast.error("Thêm vào giỏ hàng thất bại !")
    }
}

// INCREASE ITEM CART 
export const IncreaseCart = async (userId, itemId, quantity, dispatch) => {
    dispatch(increaseItemStart());
    try {
        const res = await axios.put(`${baseURL}cart/product`, { userId, itemId, quantity });
        dispatch(increaseItemSuccess(res.data));

    } catch (e) {
        dispatch(increaseItemFailed());
    }
}

// DECREASE ITEM CART
export const DecreaseCart = async (userId, itemId, quantity, dispatch) => {
    dispatch(decreaseItemStart());
    try {
        const res = await axios.put(`${baseURL}cart/decrease`, { userId, itemId, quantity })
        dispatch(decreaseItemSuccess(res.data))

    } catch (e) {
        dispatch(decreaseItemFaild())
    }
}


// DELETE ITEM CART 
export const DeleteCart = async (userId, itemId, dispatch) => {
    dispatch(deleteItemStart())
    try {
        const res = await axios.delete(`${baseURL}cart/delete`, { data: { itemId, userId } })
        dispatch(deleteItemSuccess(res.data))
        // toast.success(res.data.message)

    } catch (e) {
        dispatch(deleteItemFaild())
    }
}

// GET ALL USER
export const getAllUsers = async (accessToken, dispatch) => {
    dispatch(getUserStart());
    try {
        const res = await axios.get(`${baseURL}user/getAllUser`, {
            headers: { token: `Bearer ${accessToken}` }
        })
        dispatch(getUserSuccess(res.data));

    } catch (e) {
        dispatch(getUserFaild())
        // if (e.response.status === 403) {
        //     toast.warning(e.response.data);
        // }
    }
}

// GET ALL ORDER
export const getAllOrders = async (userId, dispatch) => {
    dispatch(getOrderStart());
    try {
        let params = {};
        if (userId) {
            params = { userId: userId };
        }
        const res = await axios.get(`${baseURL}order/getAllOrder`, {
            params: params
        });
        dispatch(getOrderSuccess(res.data));

    } catch (e) {
        dispatch(getOrderFalid());
        toast.error("Thất bại !");
    }
}

// GET ALL ROLE 
export const getAllRole = async (accessToken, dispatch) => {
    dispatch(getRoleStart())
    try {
        const res = await axios.get(`${baseURL}role/getAllRole`, {
            headers: { token: `Bearer ${accessToken}` }
        });
        dispatch(getRoleSuccess(res.data))

    } catch (e) {
        dispatch(getRoleFaild())
        if (e.response.status === 403) {
            toast.warning(e.response.data);
        }
    }
}

// GET ALL FAVOURITE
export const getAllFavorite = async (userId, dispatch) => {
    dispatch(getFavouriteStart())
    try {
        const res = await axios.get(`${baseURL}favorite/getAllFavorite`, {
            params: userId
        })

        dispatch(getFavouriteSuccess(res.data))

    } catch (e) {
        dispatch(getFavouriteFaild())
    }
}

// ADD WISH LIST
export const addNewWishList = async (newWishList, dispatch) => {
    dispatch(getFavouriteStart());
    try {
        const res = await axios.post(`${baseURL}favorite/addToFavorite`, newWishList)
        dispatch(getFavouriteSuccess(res.data))
        toast.success("Thêm vào danh sách thành công ")

    } catch (e) {
        dispatch(getFavouriteFaild());
        toast.warning("Thêm vào danh sách yêu thích thất bại !")
    }
}

// DELETE ITEM WISH LIST
export const deleteFavorite = async (userId, itemId, dispatch) => {
    dispatch(deleteWishStart())
    try {
        const res = await axios.delete(`${baseURL}favorite/deleteFavorite`, { data: { itemId, userId } })
        console.log(res.data)
        dispatch(deleteWishSuccess(res.data));
        toast.success("Xóa khỏi danh sách thành công !")

    } catch (e) {
        dispatch(deleteWishFaild())
        toast.warning("Xóa thất bại !")
    }
}