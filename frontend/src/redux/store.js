// import { configureStore } from '@reduxjs/toolkit'
// import productSlice from './productSlice'
// import cartSlice from './cartSlice'
// import authReducer from "./authSlice"
// import actionCartSlice from './actionCartSlice'
// import increaseCartSlice from './increaseCartSlice'
// import decreaseCartSlice from './decreaseCartSlice'
// import deleteCartSlice from './deleteCartSlice'
// import userSlice from './userSlice'
// import orderSlice from './orderSlice'
// import roleSlice from './roleSlice'

// export const store = configureStore({
//     reducer: {
//         auth: authReducer, product: productSlice, cart: cartSlice,
//         action: actionCartSlice,
//         increase: increaseCartSlice,
//         decrease: decreaseCartSlice,
//         delete: deleteCartSlice,
//         orders: orderSlice,
//         role: roleSlice,
//         users: userSlice,
//     },
// })


// config with redux persist
import { combineReducers, configureStore } from '@reduxjs/toolkit'
import {
    persistStore,
    persistReducer,
    FLUSH,
    REHYDRATE,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import authReducer from "./authSlice"
import productSlice from './productSlice'
import cartSlice from './cartSlice'
import actionCartSlice from './actionCartSlice'
import increaseCartSlice from './increaseCartSlice'
import decreaseCartSlice from './decreaseCartSlice'
import deleteCartSlice from './deleteCartSlice'
import userSlice from './userSlice'
import orderSlice from './orderSlice'
import roleSlice from './roleSlice'
import favouriteSlice from './favouriteSlice'
import removeWishListSlice from './removeWishListSlice'


const persistConfig = {
    key: 'root',
    version: 1,
    storage,
}
const rootReducer = combineReducers({
    auth: authReducer,
    users: userSlice,
    product: productSlice,
    cart: cartSlice,
    action: actionCartSlice,
    increase: increaseCartSlice,
    decrease: decreaseCartSlice,
    delete: deleteCartSlice,
    orders: orderSlice,
    role: roleSlice,
    favourite: favouriteSlice,
    remove: removeWishListSlice,
});

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }),
})

export let persistor = persistStore(store);