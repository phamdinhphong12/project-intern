import { createSlice } from '@reduxjs/toolkit'

const productSlice = createSlice({
    name: "product",
    initialState: {
        productList: {
            products: null,
            isFetching: false,
            error: false,
        }
    },
    reducers: {
        getProductStart: (state) => {
            state.productList.isFetching = true;
        },
        getProductSuccess: (state, actions) => {
            state.productList.isFetching = false;
            state.productList.products = actions.payload;
            state.productList.error = false;
        },
        getProductFailed: (state) => {
            state.productList.isFetching = false;
            state.productList.error = true
        }
    }
})

export const { getProductStart, getProductSuccess, getProductFailed } = productSlice.actions;

export default productSlice.reducer