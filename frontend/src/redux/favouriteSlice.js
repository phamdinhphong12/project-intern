import { createSlice } from '@reduxjs/toolkit'

const favoriteSlice = createSlice({
    name: "favourite",
    initialState: {
        favouriteList: {
            allFavourite: null,
            isFetching: false,
            error: false,
        },

    },
    reducers: {
        getFavouriteStart: (state) => {
            state.favouriteList.isFetching = true;
        },
        getFavouriteSuccess: (state, actions) => {
            state.favouriteList.isFetching = false;
            state.favouriteList.allFavourite = actions.payload;
            state.favouriteList.error = false;
        },
        getFavouriteFaild: (state) => {
            state.favouriteList.isFetching = false;
            state.favouriteList.error = true
        },


    }
})

export const { getFavouriteStart, getFavouriteSuccess, getFavouriteFaild } = favoriteSlice.actions;

export default favoriteSlice.reducer