import { createSlice } from '@reduxjs/toolkit'

const roleSlice = createSlice({
    name: "role",
    initialState: {
        roleList: {
            roleData: null,
            isFetching: false,
            error: false,
        }
    },
    reducers: {
        getRoleStart: (state) => {
            state.roleList.isFetching = true;
        },
        getRoleSuccess: (state, actions) => {
            state.roleList.isFetching = false;
            state.roleList.roleData = actions.payload;
            state.roleList.error = false;
        },
        getRoleFaild: (state) => {
            state.roleList.isFetching = false;
            state.roleList.error = true
        }
    }
})

export const { getRoleStart, getRoleSuccess, getRoleFaild } = roleSlice.actions;

export default roleSlice.reducer