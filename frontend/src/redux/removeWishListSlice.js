import { createSlice } from '@reduxjs/toolkit'

const removeWishListSlice = createSlice({
    name: "remove",
    initialState: {
        removeWishList: {
            itemDelete: null,
            isFetching: false,
            error: false,
        },
    },
    reducers: {
        deleteWishStart: (state) => {
            state.removeWishList.isFetching = true;
        },
        deleteWishSuccess: (state, actions) => {
            state.removeWishList.isFetching = false;
            state.removeWishList.itemDecrease = actions.payload;
            state.removeWishList.error = false;
        },
        deleteWishFaild: (state) => {
            state.removeWishList.isFetching = false;
            state.removeWishList.error = true
        }

    }
})

export const { deleteWishStart, deleteWishSuccess, deleteWishFaild } = removeWishListSlice.actions;
export default removeWishListSlice.reducer