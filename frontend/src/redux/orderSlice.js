import { createSlice } from '@reduxjs/toolkit'

const orderSlice = createSlice({
    name: "orders",
    initialState: {
        orderList: {
            orderData: null,
            isFetching: false,
            error: false,
        }
    },
    reducers: {
        getOrderStart: (state) => {
            state.orderList.isFetching = true;
        },
        getOrderSuccess: (state, actions) => {
            state.orderList.isFetching = false;
            state.orderList.orderData = actions.payload;
            state.orderList.error = false;
        },
        getOrderFalid: (state) => {
            state.orderList.isFetching = false;
            state.orderList.error = true
        }
    }
})

export const { getOrderStart, getOrderSuccess, getOrderFalid } = orderSlice.actions;

export default orderSlice.reducer