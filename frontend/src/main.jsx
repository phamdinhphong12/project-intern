import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import "./index.css"
import 'antd/dist/reset.css';
import { BrowserRouter } from 'react-router-dom';
// import { store } from "./redux/store.js"
import Backtop from './components/Backtop/index.jsx';
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from 'react-redux'

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <App />
        <Backtop />
      </BrowserRouter>
    </PersistGate>
  </Provider>

)
