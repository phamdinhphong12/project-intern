import Home from "../src/pages/Home"
import Detail from "./pages/Detail";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { Route, Routes } from "react-router-dom";
import ForgetPass from "./pages/forgetPass";
import Categories from "./pages/Categories";
import Cart from "./pages/Cart";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainLayout from "./components/LayoutDashboard";
import { Product } from "./pages/Product";
import { Account } from "./pages/Account";
import { Orders } from "./pages/Order";
import { InforUser } from "./pages/Infor";
import { Forbidden } from "./pages/Forbidden";
import OrderHistory from "./pages/OrderHistory";
import ChangePass from "./pages/ChangePass";
import { Role } from "./pages/Role";
import { DashboardPage } from "./pages/Dashboard";
import WishList from "./pages/WishList";
import UserClient from "./pages/UserClient";
import "./index.css"
import { ConfigProvider } from "antd";

function App() {
  return (
    <ConfigProvider theme={{
      token: {
        fontFamily: 'Josefin Sans, sans-serif',
      }
    }}>
      <ToastContainer autoClose={1500} theme="colored" />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/forget-password?" element={<ForgetPass />} />
        <Route path="/change-password?" element={<ChangePass />} />
        <Route path="/categories" element={<Categories />} />
        <Route path="/detail/:id" element={<Detail />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/order-history" element={<OrderHistory />} />
        <Route path="/wish-list" element={<WishList />} />
        <Route path="/user-client" element={<UserClient />} />
        <Route path="/forbidden" element={<Forbidden />} />

        {/* Dashboard */}
        <Route path="/dashboard" element={<MainLayout />}>
          <Route exact path="/dashboard/" element={<DashboardPage />} />
          <Route exact path="/dashboard/product" element={<Product />} />
          <Route exact path="/dashboard/account" element={<Account />} />
          <Route exact path="/dashboard/order" element={<Orders />} />
          <Route exact path="/dashboard/infor" element={<InforUser />} />
          <Route exact path="/dashboard/role" element={<Role />} />
          <Route exact path="/dashboard/forbidden" element={<Role />} />
        </Route>
      </Routes>
    </ConfigProvider >
  )
}

export default App
