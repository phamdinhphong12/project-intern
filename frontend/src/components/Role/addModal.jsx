import { Col, Form, Input, Modal, Row, Select } from 'antd';
import axios from 'axios';
import { useRef, useState } from 'react';
import { toast } from 'react-toastify';
const { Option } = Select;

const AddModal = ({ data, setData, getAll }) => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const [title, setTitle] = useState("");
    const [status, setStatus] = useState("");
    const [description, setDescription] = useState("");

    const formRef = useRef(null)

    const handleStatus = (value) => {
        setStatus(value)
    }

    const onClose = () => {
        setData(false);
        reset_form()
    };

    const createRole = () => {
        formRef.current.validateFields().then(async () => {
            const newRole = {
                roleName: title,
                description: description,
                status: status
            }
            try {
                const response2 = await axios.post(`${baseURL}role/addNewRole`, newRole);
                console.log(response2)
                setData(false);
                toast.success("Thêm mới quyền thành công");
                reset_form()
                getAll();
            } catch (e) {
                console.log("Lỗi rồi:", e)
                toast.warning("Thêm mới quyền thất bại !");
            }
        })
    }

    const reset_form = () => {
        formRef.current.resetFields();
        setTitle("");
        setDescription("");
        setStatus("")
    }


    return (
        <>
            <Modal
                title="Thêm mới nhóm quyền"
                centered
                open={data}
                onOk={createRole}
                onCancel={onClose}
                width={900}
                bodyStyle={{
                    paddingBottom: 80,
                }}
            >
                <Form ref={formRef} onFinish={createRole} layout="vertical">
                    <Row gutter={24}>
                        <Col span={24}>
                            <Form.Item
                                name="name"
                                label="Tên quyền"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Xin vui lòng nhập tên',
                                    },
                                ]}
                            >
                                <Input value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Nhập tên nhóm quyền" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="description"
                                label="Mô tả quyền"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Xin vui lòng nhập mô tả',
                                    },
                                ]}
                            >
                                <Input.TextArea value={description} onChange={(e) => setDescription(e.target.value)} rows={4} placeholder="Nhập mô tả" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="status"
                                label="Trạng thái"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng chọn trạng thái',
                                    },
                                ]}
                            >
                                <Select placeholder="Chọn trạng thái" onChange={handleStatus}>
                                    <Option value="active">Kích hoạt</Option>
                                    <Option value="inactive">Chưa kích hoạt</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};
export default AddModal;