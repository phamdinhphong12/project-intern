import { Col, Form, Input, Modal, Row, Select } from 'antd';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
const { Option } = Select;


const UpdateModal = ({ data, setData, getAll, item, trigger }) => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const [title, setTitle] = useState("");
    const [status, setStatus] = useState("");
    const [description, setDescription] = useState("");

    const [form] = Form.useForm()

    useEffect(() => {
        form.setFieldsValue({
            roleName: item?.roleName,
            description: item?.description,
            status: item?.status
        })
        setTitle(item?.roleName);
        setDescription(item?.description);
        setStatus(item?.status)


    }, [item, form, trigger])

    const handleStatus = (value) => {
        setStatus(value)
    }

    const handleUpdate = () => {
        form.validateFields().then(async () => {
            const newUpdate = {
                roleName: title,
                description: description,
                status: status
            }

            try {
                await axios.put(`${baseURL}role/update/${item?._id}`, newUpdate)
                setData(false);
                toast.success("Cập nhật quyền thành công");
                reset_form();
                getAll()

            } catch (e) {
                console.log("Err:", e);
                toast.warning("Cập nhật quyền thất bại !")
            }
        })

    }


    const onClose = () => {
        setData(false);
        reset_form()
    };


    const reset_form = () => {
        form.resetFields();
        setTitle("");
        setDescription("");
        setStatus("")
    }


    return (
        <>
            <Modal
                title="Cập nhật nhóm quyền"
                centered
                open={data}
                onOk={handleUpdate}
                onCancel={onClose}
                width={900}
                bodyStyle={{
                    paddingBottom: 80,
                }}

            >
                <Form form={form} onSubmit={handleUpdate} layout="vertical">
                    {/* {JSON.stringify(item)} */}
                    <Row gutter={24}>
                        <Col span={24}>
                            <Form.Item
                                name="roleName"
                                label="Tên quyền"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Xin vui lòng nhập tên',
                                    },
                                ]}
                            >
                                <Input value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Nhập tên nhóm quyền" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="description"
                                label="Mô tả"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mô tả',
                                    },
                                ]}
                            >
                                <Input.TextArea value={description} onChange={(e) => setDescription(e.target.value)} rows={4} placeholder="Nhập mô tả" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="status"
                                label="Trạng thái"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng chọn trạng thái',
                                    },
                                ]}
                            >
                                <Select placeholder="Chọn trạng thái" onChange={handleStatus}>
                                    <Option value="active">Kích hoạt</Option>
                                    <Option value="inactive">Chưa kích hoạt</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};
export default UpdateModal;