import { PlusOutlined, LoadingOutlined } from '@ant-design/icons';
import { Button, Col, Drawer, Form, Input, Modal, Row, Select, Space, Upload, message } from 'antd';
import { useState, useRef } from 'react';
import { toast } from 'react-toastify';
import axios from 'axios';
const { Option } = Select;

// const getBase64 = (img, callback) => {
//     const reader = new FileReader();
//     reader.addEventListener('load', () => callback(reader.result));
//     reader.readAsDataURL(img);
// }

const beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
};


const AddModal = ({ data, setData, getAll }) => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const urlApiImg = `${baseURL}upload`
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState();
    const [code, setCode] = useState("")
    const [price, setPrice] = useState();
    const [title, setTitle] = useState("");
    const [status, setStatus] = useState("");
    const [description, setDescription] = useState("");

    const formRef = useRef(null)

    const handleStatus = (value) => {
        setStatus(value)
    }


    const handleChange = (info) => {
        // console.log(info)
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
            // Set imageUrl to the uploaded image URL
            setImageUrl(info.file.response.imageURL);
            console.log(info.file.response.imageURL)
            setLoading(false);
        } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
            setLoading(false);
        }
    };
    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </div>
    )

    const onClose = () => {
        setData(false);
        reset_form()
    };

    const createProducts = () => {
        formRef.current.validateFields().then(async () => {
            const newProduct = {
                code: code,
                title: title,
                img: imageUrl,
                price: price,
                description: description,
                status: status
            }
            try {
                const response2 = await axios.post(`${baseURL}product/addNew`, newProduct);
                console.log(response2)
                setData(false);
                toast.success("Thêm mới sản phẩm thành công");
                reset_form()
                getAll();
            } catch (e) {
                console.log("Lỗi rồi:", e)
                toast.warning("Thêm mới đồ uống thất bại !");
            }
        })
    }

    const reset_form = () => {
        formRef.current.resetFields();
        setImageUrl("");
        setPrice("");
        setTitle("");
        setDescription("");
        setStatus("")
    }


    return (
        <>
            <Modal
                title="Thêm mới sản phẩm"
                centered
                open={data}
                onOk={createProducts}
                onCancel={onClose}
                width={900}
                bodyStyle={{
                    paddingBottom: 80,
                }}
            >
                <Form ref={formRef} onFinish={createProducts} layout="vertical">
                    <Row style={{ justifyContent: "center", marginBottom: 20 }}>
                        <Col>
                            <Upload
                                name="file"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                beforeUpload={beforeUpload}
                                action={urlApiImg}
                                onChange={handleChange}

                            >
                                {imageUrl ? (
                                    <img
                                        src={imageUrl}
                                        alt="avatar"
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                            objectFit: 'cover'
                                        }}
                                    />
                                ) : (
                                    uploadButton
                                )}
                            </Upload>
                        </Col>

                    </Row>
                    <Row gutter={16}>
                        <Col span={8}>
                            <Form.Item
                                name="code"
                                label="Mã sản phẩm"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Xin vui lòng nhập mã',
                                    },
                                ]}
                            >
                                <Input type="type" value={code} onChange={(e) => setCode(e.target.value)} placeholder="Nhập mã" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item
                                name="name"
                                label="Tên sản phẩm"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Xin vui lòng nhập tên',
                                    },
                                ]}
                            >
                                <Input value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Nhập tên đồ uống" />
                            </Form.Item>
                        </Col>
                        <Col span={8}>
                            <Form.Item
                                name="price"
                                label="Giá sản phẩm"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Xin vui lòng nhập giá',
                                    },
                                ]}
                            >
                                <Input value={price} onChange={(e) => setPrice(e.target.value)} type="number" placeholder="Nhập giá đồ uống" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="description"
                                label="Mô tả sản phẩm"
                            >
                                <Input.TextArea value={description} onChange={(e) => setDescription(e.target.value)} rows={4} placeholder="Nhập mô tả" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row gutter={16}>
                        <Col span={24}>
                            <Form.Item
                                name="status"
                                label="Trạng thái"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng chọn trạng thái',
                                    },
                                ]}
                            >
                                <Select placeholder="Chọn trạng thái" onChange={handleStatus}>
                                    <Option value="active">Còn hàng</Option>
                                    <Option value="inactive">Hết hàng</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Modal>
        </>
    );
};
export default AddModal;