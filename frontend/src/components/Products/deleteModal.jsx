import React from 'react'
import { Modal } from 'antd';
import { toast } from 'react-toastify';
import axios from 'axios';


export const DeleteProducts = ({ open, setOpen, item, getAll }) => {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;

    const ok = async () => {
        try {
            await axios.delete(`${baseURL}product/${item?._id}`)
            setOpen(false)
            toast.success("Xóa sản phẩm thành công")
            getAll()

        } catch (e) {
            console.log("ERR:", e)
            toast.warning("Xóa sản phẩm thất bại !");
        }
    }

    return (
        <>
            <Modal
                title="Delete"
                centered
                open={open}
                onOk={ok}
                onCancel={() => setOpen(false)}
            >
                <p style={{ textAlign: "center" }}>Bạn có chắc muốn xóa sản phẩm <span style={{ color: "red" }}>{JSON.stringify(item?.title)}</span> này ?</p>
            </Modal>
        </>
    );
}
