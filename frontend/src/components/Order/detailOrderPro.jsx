import { Col, Divider, Modal, Row, Tag, Image } from 'antd';
import moment from 'moment';
import React from 'react';
import Money from '../Money';


export const DetailOrderProducts = ({ open, setOpen, item }) => {

    return (
        <>
            <Modal
                title="Các sản phẩm đã đặt"
                centered
                width={1300}
                open={open}
                onOk={() => setOpen(false)}
                onCancel={() => setOpen(false)}
            >
                <Col style={{ maxHeight: '700px', width: '1600px', overflowY: 'auto' }}>
                    <Col style={{ margin: '30px 100px', }}>
                        <Col style={{ width: '2000px' }}>
                            {item?.order_products.map((value, i) => (
                                <Col span={24}>
                                    <Col span={24} key={i} >
                                        {value.item.map((vl, id) => (
                                            <Row style={{ justifyContent: 'space-between', alignItems: "center" }}>
                                                <Col key={id}>
                                                    <br />
                                                    <span>Tên sản phẩm: <b style={{ color: '#e83e8c' }}>{vl.name}</b> </span>
                                                    <br />
                                                    <span>Kích cỡ: <b>{vl.size?.join(', ')}</b></span>
                                                    <br />
                                                    <span>Số lượng: <b>{vl.quantity}</b></span>
                                                    <br />
                                                    <span>Ghi chú sản phẩm: <b>{vl.note}</b></span>
                                                </Col>
                                                <Col>
                                                    <Image src={vl.avatar} width={100} />
                                                </Col>
                                                <Divider />
                                            </Row>

                                        ))}
                                    </Col>
                                    <br />
                                    <Row style={{ justifyContent: 'space-between', alignItems: "center" }}>
                                        <Col>
                                            <h2>
                                                <b>Tổng số lượng:</b>
                                            </h2>
                                        </Col>
                                        <Col>
                                            <h2>
                                                <b style={{ color: '#e83e8c' }}>{value.quantity}</b>
                                            </h2>
                                        </Col>
                                    </Row>
                                    <br />
                                    <Row style={{ justifyContent: 'space-between', alignItems: "center" }}>
                                        <Col>
                                            <h2>
                                                <b>Thành tiền: </b>
                                            </h2>
                                        </Col>
                                        <Col>  <Tag color='green'><Money value={value.price} /></Tag></Col>
                                    </Row>
                                    <br />
                                </Col>

                            ))}
                        </Col>
                    </Col>
                </Col>
            </Modal>
        </>
    );
}
