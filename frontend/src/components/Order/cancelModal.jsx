import React, { useState } from 'react'
import { Modal } from 'antd';
import { toast } from 'react-toastify';
import axios from 'axios';


export const CancelOrder = ({ open, setOpen, item, getAll }) => {
    const [cancelStatus, setCancelStatus] = useState(["cancel"])
    const baseURL = import.meta.env.VITE_API_PRODUCTS;

    const OK = async () => {
        const newOrder = {
            order_products: item?.order_products,
            customer_name: item?.customerName,
            customer_phone: item?.customerPhone,
            customer_address: item?.customerAddress,
            order_pay: item?.orderPay,
            order_description: item?.orderDes,
            order_status: cancelStatus,
        }
        try {
            await axios.put(`${baseURL}order/update/${item?._id}`, newOrder);
            toast.success("Hủy đơn hàng thành công")
            setOpen(false);
            getAll();

        } catch (e) {
            toast.warning("Hủy đơn hàng thất bại !")
            console.log("Err", e)
        }
    }

    return (
        <>
            <Modal
                title="Cảnh báo"
                centered
                open={open}
                onOk={OK}
                onCancel={() => setOpen(false)}
            >
                <p style={{ textAlign: "center" }}>Bạn có chắc muốn hủy đơn hàng <span style={{ color: "red" }}>{JSON.stringify(item?._id)}</span> này ?</p>
            </Modal>
        </>
    );
}
