import React from 'react';
import { PayPalButtons, PayPalScriptProvider } from "@paypal/react-paypal-js";

export const Paypal = ({ handleOrder, cartItem, totalPrice }) => {

    // Thanh toán Paypal
    // format $
    const convertToUSD = (vndAmount) => {
        const exchangeRate = 23000; // Tỷ giá hối đoái
        const usdAmount = (vndAmount / exchangeRate).toFixed(2); // Tính giá trị tương ứng trong USD
        return usdAmount;
    }

    const ItemCart = cartItem.map((item) => item.product)

    const createOrder = (data, actions) => {
        return actions.order.create({
            purchase_units: [
                {
                    description: `Thanh toán sản phẩm này: ${ItemCart.join(', ')}`,
                    amount: {
                        value: convertToUSD(totalPrice)
                    },
                },
            ],
        });
    }

    const onApprove = (data, actions) => {
        return actions.order.capture()
            .then(function () {
                handleOrder();
            })
            .catch(error => console.error('Lỗi trong onApprove:', error));
    }



    return (
        <PayPalScriptProvider options={{ "client-id": "AWJQdrLhlyUPKQANn9J848hR0SxzOtJu9T_lVUt3qjuc9fzEjjMbpyMcvDmVx_I2dLAiyytdLhDm7Zbx" }}>
            <PayPalButtons createOrder={createOrder} onApprove={onApprove} />
        </PayPalScriptProvider>
    )
}
