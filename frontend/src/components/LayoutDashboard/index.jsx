import { AppstoreOutlined, BellOutlined, CarryOutOutlined, CoffeeOutlined, DashboardOutlined, DollarOutlined, MenuOutlined, NodeExpandOutlined, SettingOutlined, StarOutlined, UserOutlined } from '@ant-design/icons';
import { Avatar, Col, Dropdown, Grid, Layout, Menu, Row, Typography, theme } from 'antd';
import SubMenu from 'antd/es/menu/SubMenu';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, Route, Routes, useLocation } from "react-router-dom";
import "../../CSS/LayoutDash.css";
import { Account } from "../../pages/Account";
import { DashboardPage } from '../../pages/Dashboard';
import { Forbidden } from '../../pages/Forbidden';
import { InforUser } from '../../pages/Infor';
import { Orders } from '../../pages/Order';
import { Product } from '../../pages/Product';
import { Role } from '../../pages/Role';
import { Logout } from '../Logout';


const { Header, Content, Footer, Sider } = Layout;
const { Title } = Typography;
const { useBreakpoint } = Grid;


const MainLayout = () => {
    const [collapsed, setCollapsed] = useState(false);
    // const { user, setUser } = useContext(AuthContext)
    const user = useSelector((state) => state.auth.login?.currentUser.user)
    const screens = useBreakpoint();
    // console.log(user)
    const [openLogout, setOpenLogOut] = useState(false)
    const location = useLocation();

    const getPageTitle = () => {
        switch (location.pathname) {
            case "/main/home":
                return "Trang chủ";
            case "/dashboard/product":
                return "Quản lý sản phẩm";
            case "/dashboard/account":
                return "Quản lý tài khoản";
            case "/dashboard/order":
                return "Quản lý đơn hàng";
            case "/dashboard/role":
                return "Tạo quyền"
            case "/dashboard/infor":
                return "Thông tin tài khoản";
            case "/dashboard/forbidden":
                return "403"
            default:
                return "Dashbroad";
        }
    };

    const handleLogout = () => {
        setOpenLogOut(true)
    }

    const itemsDropAvatar = [
        {
            key: '1',
            label: (
                <Link to="/dashboard/infor" >Thông tin người dùng</Link>
            ),
        },
        {
            key: '2',
            label: (
                <Link to="/dashboard/account"> Tài khoản</Link>
            ),
        },
        {
            key: '3',
            label: (
                <Link to="/"> Home</Link>
            ),
        },
        {
            key: '4',
            label: (
                <span onClick={handleLogout}>Đăng xuất</span>
            ),
        },
    ];
    const itemsDropNotification = [
        {
            key: '1',
            label: (
                <span> Chưa có thông báo !</span>
            ),
        },
    ];

    const {
        token: { colorBgContainer },
    } = theme.useToken();

    // useEffect(() => {
    //     toast('✨ Welcome to Dashboard ✨ ', {
    //         position: "top-right",
    //         autoClose: 1000,
    //         theme: "dark",
    //     });
    // }, [])
    return (
        <Layout
            style={{
                minHeight: '100vh',
            }}
        >
            <Sider trigger={null} collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)} width={210} style={{ background: "#e83e8c" }}>
                {collapsed === false && <Row style={{ padding: "10px 0px", justifyContent: "center" }}>
                    <Col style={{ width: 20 }}>
                        <StarOutlined width={20} style={{ color: "white" }} />
                    </Col>
                    <Col>
                        <h1 style={{ color: 'white' }}>Wind Store</h1>
                    </Col>
                </Row>}

                {/* Menu side bar */}
                {/* <Menu theme="dark" defaultSelectedKeys={['1']} mode="inline" items={items} /> */}
                <Menu theme="light" mode="inline" style={{ background: "#e83e8c", color: "white" }}>
                    <Menu.Item key="1" icon={<DashboardOutlined />}>
                        <Link to="/dashboard">Trang chủ</Link>
                    </Menu.Item>
                    <SubMenu key="sub1" icon={<AppstoreOutlined />} title="Quản lý hệ thống">
                        <Menu.Item key="2" icon={<CoffeeOutlined />}>
                            <Link to="/dashboard/product">Sản phẩm</Link>
                        </Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub2" icon={<NodeExpandOutlined />} title="Đơn hàng">
                        <Menu.Item key="5" icon={<DollarOutlined />}>
                            <Link to="/dashboard/order">Quản lý đơn hàng</Link>
                        </Menu.Item>
                    </SubMenu>
                    <SubMenu key="sub3" icon={<UserOutlined />} title="Quản lý tài khoản">
                        <Menu.Item key="9" icon={<UserOutlined />}>
                            <Link to="/dashboard/account">Tài khoản</Link>
                        </Menu.Item>
                        <Menu.Item key="10" icon={<SettingOutlined />}>
                            <Link to="/dashboard/role">Tạo quyền</Link>
                        </Menu.Item>
                    </SubMenu>
                    <Menu.Item key="11" icon={<CarryOutOutlined />}>
                        <Link to="/dashboard">Thống kê</Link>
                    </Menu.Item>
                    {screens.xs ? <Col>
                        <Row style={{ alignItems: 'center', marginRight: 20 }}>
                            <Col style={{ marginLeft: 25 }}>
                                <Col>
                                    <Row style={{ alignItems: 'center' }}>
                                        <Dropdown
                                            overlay={<Menu items={itemsDropAvatar} />}
                                            placement="bottom"
                                            arrow
                                            trigger={['click']}
                                        >
                                            {user.avatar ? <Avatar src={user.avatar} /> : <Avatar icon={<UserOutlined />} />}
                                        </Dropdown>
                                        {collapsed === false && <Col style={{ marginLeft: 10, marginTop: 8 }}>
                                            <Title level={5}>{user.username}</Title>
                                        </Col>}
                                        
                                    </Row>
                                </Col>
                            </Col>
                        </Row>
                    </Col> : ''}
                    
                </Menu>
            </Sider>
            <Layout className="site-layout">
                <Header
                    style={{
                        padding: 0,
                        background: colorBgContainer,
                    }}
                >
                    <Row style={{ justifyContent: "space-between" }}>
                        <Col>
                            <Row>
                                <Col style={{ marginLeft: 20 }}>
                                    <MenuOutlined onClick={() => setCollapsed(!collapsed)} />
                                </Col>

                                <Col style={{ marginLeft: 20, marginTop: 17 }}>
                                    <Title level={4}>{getPageTitle()}</Title>
                                </Col>

                            </Row>
                        </Col>
                        {screens.xs ? '' : <Col>
                            <Row style={{ alignItems: 'center', marginRight: 20 }}>
                                <Col>
                                    <Dropdown
                                        overlay={<Menu items={itemsDropNotification} />}
                                        placement="bottom"
                                        arrow
                                        trigger={['click']}
                                    >
                                        <BellOutlined style={{ fontSize: 18 }} />

                                    </Dropdown>
                                </Col>
                                <Col style={{ marginLeft: 25 }}>
                                    <Col>
                                        <Row style={{ alignItems: 'center' }}>
                                            <Dropdown
                                                overlay={<Menu items={itemsDropAvatar} />}
                                                placement="bottom"
                                                arrow
                                                trigger={['click']}
                                            >
                                                {user.avatar ? <Avatar src={user.avatar} /> : <Avatar icon={<UserOutlined />} />}
                                            </Dropdown>
                                            <Col style={{ marginLeft: 10, marginTop: 8 }}>
                                                <Title level={5}>{user.username}</Title>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Col>
                            </Row>

                        </Col>}
                    
                    </Row>
                </Header>


                <Content
                    style={{
                        margin: '0 16px',
                    }}
                >
                    {/* Router các page */}
                    <Routes>
                        {/* <Route exact path="/home" element={<HomePage />} /> */}
                        <Route path="/" element={<DashboardPage />} />
                        <Route path="/product" element={<Product />} />
                        <Route path="/account" element={<Account />} />
                        <Route path="/order" element={<Orders />} />
                        <Route path="/infor" element={<InforUser />} />
                        <Route path="/role" element={<Role />} />
                        <Route path="/forbidden" element={<Forbidden />} />
                    </Routes>

                    <Logout open={openLogout} setOpen={setOpenLogOut} name={user.username} />
                </Content>
                <Footer
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Design ©2023 Created by The Wind
                </Footer>
            </Layout>
        </Layout>
    );
};
export default MainLayout;