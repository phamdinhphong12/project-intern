import React from "react";
import { BackTop, Col, FloatButton } from "antd";
import { UpCircleOutlined } from "@ant-design/icons";

export default function Backtop() {
    return (
        <div>
            <FloatButton.BackTop>
                <Col
                    style={{
                        height: 50,
                        width: 50,
                        fontSize: 30,
                        color: "#e83e8c",
                        textAlign: "center",
                    }}
                >
                    <UpCircleOutlined />
                </Col>
            </FloatButton.BackTop>
        </div>
    );
}
