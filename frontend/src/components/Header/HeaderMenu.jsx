import { UserOutlined } from '@ant-design/icons';
import { Dropdown, Menu, Row, Typography } from 'antd';
import Link from 'antd/es/typography/Link';
import React from "react";
import { useNavigate } from 'react-router';
import { LogoutUser } from '../../API/apiRequest';
import Categories from '../../pages/Categories';
import { DashboardPage } from '../../pages/Dashboard';
import Home from '../../pages/Home';

export const HeaderMenu = ({ user, id, accessToken, setCartLength, setWishLength }) => {
    const { Title } = Typography
    const navigate = useNavigate()

    const menuItems = [
        {
            label: "Home",
            path: '/',
        },
        {
            label: 'Our Shop',
            path: '/categories',
        },
        {
            label: 'On Sale',
            path: '/on-sale',
        },
        {
            label: 'Our Services',
            path: '/services',
        },
        {
            label: 'Blog',
            path: '/blog',
        },
        {
            label: 'Dashboard',
            path: user?.user.Role === "ADMIN" || user?.user.Role === "MANAGER" || user?.user.Role === "STAFF" ? "/dashboard" : '/forbidden',
        },
    ]
    const items = [
        {
            label: " US.Dollar",
            key: '0',
        },
        {
            label: 'RTGS Dollar',
            key: '1',
        },
        {
            label: 'SA Rand',
            key: '2',
        },
    ];

    const handleLogout = () => {
        LogoutUser(id, dispatch, navigate, accessToken)
        setCartLength(0)
        setWishLength(0)
    }

    const handleMyAccount = () => {
        navigate("/user-client")
    }

    const handleChangePass = () => {
        navigate("/change-password?")
    }

    const dropUser = [
        {
            key: '0',
            label: (
                <span onClick={() => navigate("/order-history")}>Order</span>
            ),
        },
        {
            key: '1',
            label: (
                <span onClick={handleMyAccount}>My Account</span>
            ),
        },
        {
            key: '2',
            label: (
                <span onClick={handleChangePass}>Change Password</span>
            ),
        },
        {
            key: '3',
            label: (
                <span onClick={handleLogout}>Sign Out</span>
            ),
        },
    ]

    return (
        <Row className='menu_item'>
            {menuItems.map((menuItem, index) => (
                <Link key={index} href={menuItem.path}>
                    <Title className='menu_item_link' level={5} style={{ cursor: 'pointer', color: "white" }}>{menuItem.label}</Title>
                </Link>
            ))}
            {user ? (
                <Link>
                    <Dropdown
                        overlay={<Menu items={dropUser} />}
                        placement="bottom"
                        arrow
                        trigger={['click']}
                    >
                        <Title className='menu_item_link' level={5} style={{ cursor: 'pointer', color: "white" }} ><UserOutlined /> {user?.user.username} </Title>
                    </Dropdown>
                </Link>
            ) : (
                <Link href='/login'>
                    <Title className='menu_item_link' level={5} style={{ cursor: 'pointer', color: "white" }} >Login</Title>
                </Link>
            )}
        </Row>


    )
}