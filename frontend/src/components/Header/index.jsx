import { DownOutlined, HeartOutlined, MenuFoldOutlined, ShoppingOutlined, StarOutlined } from '@ant-design/icons';
import { Badge, Col, Dropdown, Grid, Input, Row, Space } from 'antd';
import Link from 'antd/es/typography/Link';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCart, getAllFavorite } from "../../API/apiRequest.js";
import "../../CSS/Header.css";
import HeaderMobile from '../HeaderMobile/index.jsx';
import { HeaderMenu } from './HeaderMenu.jsx';

const { useBreakpoint } = Grid;

export default function Header() {

    const dispatch = useDispatch();
    const [cartLength, setCartLength] = useState(0)
    const [wishLength, setWishLength] = useState(0)
    const screens = useBreakpoint();
    const [collapsed, setCollapsed] = useState(true);

    const toggleCollapsed = () => {
        setCollapsed(!collapsed);
    }

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const dataCart = useSelector((state) => state.cart.cartList?.allCart);
    const dataFavourite = useSelector((state) => state.favourite.favouriteList?.allFavourite)

    const userId = {
        userId: user?.user._id
    }
    const getCart = async () => {
        getAllCart(userId, dispatch);
    }
    const getAllFavoritess = async () => {
        getAllFavorite(userId, dispatch)
    }



    const id = user?._id
    const accessToken = user?.accessToken

    const items = [
        {
            label: " US.Dollar",
            key: '0',
        },
        {
            label: 'RTGS Dollar',
            key: '1',
        },
        {
            label: 'SA Rand',
            key: '2',
        },
    ];

    useEffect(() => {
        getCart()
        getAllFavoritess()
    }, [user])

    useEffect(() => {
        setCartLength(dataCart.length)
        setWishLength(dataFavourite.length)

    }, [dataCart, dataFavourite])


    return (
        <>
            <HeaderMobile
                user={user}
                collapsed={collapsed}
                toggleCollapsed={toggleCollapsed}
                id={id}
                accessToken={accessToken}
                wishLength={wishLength}
                setWishLength={setWishLength}
                cartLength={cartLength}
            />

            <Col className='header' sm={24} xl={12}>
                <Row className='header_wrapper'>
                    <Col>
                        <h2><StarOutlined style={{ color: '#e83e8c' }} /> Wind Store</h2>
                    </Col>
                    <Col span={8}>
                        <Input className='header_search' placeholder='Search...' />
                    </Col>

                    {(screens.md) ?
                        <>
                            <Col>
                                <Dropdown
                                    menu={{
                                        items
                                    }}
                                    trigger={['click']}
                                >
                                    <Space>
                                        <span style={{ cursor: 'pointer' }}>US.Dollar</span>
                                        <DownOutlined />
                                    </Space>
                                </Dropdown>
                            </Col>
                            <Col>
                                <Link href='/wish-list' className='header_link' style={{ color: "black" }}>
                                    <Badge count={wishLength} showZero size="small" >
                                        <HeartOutlined style={{ marginRight: 10 }} />
                                    </Badge>
                                    <span style={{ marginLeft: 15 }}>wishlist</span>
                                </Link>
                                <Link style={{ color: "black" }} href='/cart'>
                                    <Badge count={cartLength} showZero size='small'>
                                        <ShoppingOutlined style={{ marginRight: 10 }} />
                                    </Badge>
                                    <span style={{ marginLeft: 15 }}>shopping card</span>
                                </Link>
                            </Col>
                        </> : ''}
                    {(screens.md) ? '' :
                        <Col>
                            <MenuFoldOutlined style={{ fontSize: '20px' }} onClick={() => toggleCollapsed()} />
                        </Col>
                    }
                </Row>
            </Col>

            {(screens.md) ? <Col span={24} className='menu' style={{ background: "#282828" }}>
                <HeaderMenu
                    items={items}
                    user={user}
                    id={id}
                    accessToken={accessToken}
                    setWishLength={setWishLength}
                    setCartLength={setCartLength}
                />
            </Col> : ''}
        </>

    )
}
