import { Col, Row, Typography, Input, Divider, Grid } from 'antd'
import { InstagramOutlined, TwitterOutlined, YoutubeFilled, LinkedinFilled, PlayCircleOutlined, FacebookFilled } from '@ant-design/icons';
import React from 'react'
import "../../CSS/Footer.css"
import Link from 'antd/es/typography/Link';


const { useBreakpoint } = Grid;

export default function Footer() {
    const { Search } = Input;
    const { Title } = Typography
    const screens = useBreakpoint();
    return (
        <>
            <Col className='wrapper_all' span={24} style={{ background: "#282828", padding: 10 }}>
                <Row className='footer_title' style={{ gap: screens.xs ? 10 : 'unset' }}>
                    <Col xxl={6} sm={10} xs={24}>
                        <Title style={{ color: 'white' }} level={5}>ABOUT</Title>
                        <span className='footer_text'>Online & physical bead shop with the best beads and beading supplies in Zimbabwe
                            ✓ Over 9000 beads for jewelry making ✓ Glass beads ✓
                            Beading supplies and much more!</span>
                        {/* <img className='footer_img' src='https://seamaf.com/frontend/img/cards.png' /> */}

                        <Search
                            className='footer_subcirbe'
                            placeholder="Enter e-mail"
                            allowClear
                            enterButton="SUBCRIBE"
                            size="small"
                        />
                    </Col>
                    <Col xxl={6} sm={10} xs={24}>
                        <Title style={{ color: 'white' }} level={5}>USEFUL LINKS</Title>
                        <Row className='footer_links'>
                            <Col>
                                <Col className='links_1'>
                                    <Link style={{ color: "#8f8f8f" }}>About Us</Link>
                                </Col>
                                <Col className='links_1'>
                                    <Link style={{ color: "#8f8f8f" }}>Track Order</Link>
                                </Col >
                                <Col className='links_1'>
                                    <Link style={{ color: "#8f8f8f" }}>Shipping</Link>
                                </Col>
                                <Col className='links_1'>
                                    <Link style={{ color: "#8f8f8f" }}>Contact</Link>
                                </Col>
                                <Col className='links_1'>
                                    <Link style={{ color: "#8f8f8f" }}>My orders</Link>
                                </Col>
                            </Col>
                            <Col>
                                <Col className='links_2'>
                                    <Link style={{ color: "#8f8f8f" }}>Support</Link>
                                </Col>
                                <Col className='links_2'>
                                    <Link style={{ color: "#8f8f8f" }}>Terms of Use</Link>
                                </Col>
                                <Col className='links_2'>
                                    <Link style={{ color: "#8f8f8f" }}>Privacy policy</Link>
                                </Col>
                                <Col className='links_2'>
                                    <Link style={{ color: "#8f8f8f" }}>Our Service</Link>
                                </Col>
                                <Col className='links_2'>
                                    <Link style={{ color: "#8f8f8f" }}>Blog</Link>
                                </Col>
                            </Col>
                        </Row>
                        <Col style={{ marginTop: 20 }}></Col>
                    </Col>
                    <Col xxl={6} sm={10} xs={24}>
                        <Title style={{ color: 'white' }} level={5}>BLOG</Title>
                        <Col style={{ marginTop: 20 }}>
                            <Row>
                                <Col>
                                    <img className='blog_img' src='https://i.pinimg.com/originals/4d/eb/ce/4debce7b35b95e0ebc67890b2093c672.jpg' />
                                </Col>
                                <Col>
                                    <h4 style={{ color: 'white', width: 140 }}>BOHE MIAN WEDDING THEME</h4>
                                    <span style={{ color: "#8f8f8f" }}>2 years ago</span> <br />
                                    <Link style={{ color: "red" }} >Read more</Link>
                                </Col>
                            </Row>

                        </Col>
                        <Col style={{ marginTop: 30 }}>
                            <Row>
                                <Col>
                                    <img className='blog_img' src='https://i.pinimg.com/originals/4d/eb/ce/4debce7b35b95e0ebc67890b2093c672.jpg' />
                                </Col>
                                <Col >
                                    <h4 style={{ color: 'white', width: 140 }}>BOHE MIAN WEDDING THEME</h4>
                                    <span style={{ color: "#8f8f8f" }}>2 years ago</span> <br />
                                    <Link style={{ color: "red" }}>Read more</Link>
                                </Col>
                            </Row>

                        </Col>
                    </Col>
                    <Col xxl={6} sm={10} xs={24}>
                        <Title style={{ color: 'white' }} level={5}>CONTACT</Title>
                        <Col style={{ marginTop: 10 }}>
                            <Row>
                                <span className='contact_title'>C.</span>
                                <p style={{ color: "#8f8f8f" }}>ilubooks</p>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <span className='contact_title'>B.</span>
                                <p style={{ color: "#8f8f8f", width: "88%" }}>108 Chinhoyi Street, Central Business District, Harare Zimbabwe</p>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <span className='contact_title'>T.</span>
                                <p style={{ color: "#8f8f8f" }}>+263782149840</p>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <span className='contact_title'>E.</span>
                                <p style={{ color: "#8f8f8f" }}>rvmseamaf@gmail.com</p>
                            </Row>
                        </Col>
                    </Col>
                </Row >
                <Divider />
                <Col className='wrapper_social' span={24} style={{ background: "#282828", marginTop: 20, marginBottom: 20 }}>
                    <Row style={{ alignItems: 'center', justifyContent: 'space-evenly', gap: 4, padding: (screens.xxl) ? '0 400px' : (screens.xl) ? '0 200px' : 0 }}>
                        <Col>
                            <Link style={{ color: "white" }}>
                                <InstagramOutlined />
                                <span style={{ marginLeft: 10 }}>INSTAGRAM</span>
                            </Link>
                        </Col>
                        <Col>
                            <Link style={{ color: "white" }}>
                                <PlayCircleOutlined />
                                <span style={{ marginLeft: 10 }}>
                                    PINTEREST
                                </span>
                            </Link>
                        </Col>
                        <Col>
                            <Link style={{ color: "white" }}>
                                <FacebookFilled />
                                <span style={{ marginLeft: 10 }}>
                                    FACEBOOK
                                </span>
                            </Link>
                        </Col>
                        <Col>
                            <Link style={{ color: "white" }}>
                                <TwitterOutlined />
                                <span style={{ marginLeft: 10 }}>
                                    TWITTER
                                </span>
                            </Link>
                        </Col>
                        <Col>
                            <Link style={{ color: "white" }}>
                                <YoutubeFilled style={{ width: 20 }} />
                                <span style={{ marginLeft: 10 }}>
                                    YOUTUBE
                                </span>
                            </Link>
                        </Col>
                        <Col>
                            <Link style={{ color: "white" }}>
                                <LinkedinFilled />
                                <span style={{ marginLeft: 10 }}>
                                    LINEDIN
                                </span>
                            </Link>
                        </Col>
                    </Row>
                    <Col style={{ textAlign: "center", }}>
                        <h5 style={{ color: "white", marginTop: 50, paddingBottom: 20 }}>Copyright ©2023 All rights reserved | Developed By Eloquent Geeks</h5>
                    </Col>
                </Col>
            </Col >
        </>
    )
}
