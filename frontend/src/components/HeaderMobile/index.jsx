import { HeartOutlined, ShoppingOutlined, UserOutlined } from '@ant-design/icons';
import { Badge, Layout, Menu, Typography } from 'antd';
import Link from 'antd/es/typography/Link';
import React from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { LogoutUser } from '../../API/apiRequest';

const { Sider } = Layout

export default function HeaderMobile({ collapsed, toggleCollapsed, user, id, accessToken, setCartLength, setWishLength, wishLength, cartLength }) {
    const { Title } = Typography
    const navigate = useNavigate()
    const dispatch = useDispatch()


    const handleLogout = () => {
        LogoutUser(id, dispatch, navigate, accessToken)
        setCartLength(0)
        setWishLength(0)
    }

    const handleMyAccount = () => {
        navigate("/user-client")
    }

    const handleChangePass = () => {
        navigate("/change-password?")
    }
    const dropUser = [
        {
            key: '0',
            label: (
                <span onClick={() => navigate("/order-history")}>Order</span>
            ),
        },
        {
            key: '1',
            label: (
                <span onClick={handleMyAccount}>My Account</span>
            ),
        },
        {
            key: '2',
            label: (
                <span onClick={handleChangePass}>Change Password</span>
            ),
        },
        {
            key: '3',
            label: (
                <span onClick={handleLogout}>Sign Out</span>
            ),
        },
    ]
    return (
        <Layout>
            <Sider
                collapsible
                collapsed={collapsed}
                onCollapse={toggleCollapsed}
                collapsedWidth={0}
                width={200}
                style={{
                    overflow: 'auto',
                    height: '100vh',
                    position: 'fixed',
                    left: 0,
                    zIndex: 2
                }}>
                <Menu theme="dark" mode="inline" >
                    <Menu.Item key="1" href="/">
                        <Link href='/'>
                            Home
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <Link href='/categories'>
                            Our Shop
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                        On Sale
                    </Menu.Item>
                    <Menu.Item key="4">
                        Our Services
                    </Menu.Item>
                    <Menu.Item key="5">
                        <Link href={user?.user.Role === "ADMIN" || user?.user.Role === "MANAGER" || user?.user.Role === "STAFF" ? "/dashboard" : '/forbidden'}>
                            Dashboard
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='6'>
                        <Link href='/wish-list' className='header_link'>
                            <Badge count={wishLength} showZero size="small" >
                                <HeartOutlined style={{ color: 'white' }} />
                            </Badge>
                            <span style={{ marginLeft: 15 }}>wishlist</span>
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='7'>
                        <Link href='/cart' >
                            <Badge count={cartLength} showZero size='small'>
                                <ShoppingOutlined style={{ color: 'white' }} />
                            </Badge>
                            <span style={{ marginLeft: 15, color: 'white' }}>shopping card</span>
                        </Link>
                    </Menu.Item>


                    {user ?
                        <Menu.SubMenu key="8" title={
                            <span>
                                <UserOutlined />{" "}
                                {user?.user.username}
                            </span>
                        }>
                            {dropUser.map(item => (
                                <Menu.Item key={item.key}>{item.label}</Menu.Item>
                            ))}
                        </Menu.SubMenu> :
                        <Menu.Item key='8'>
                            <Link href='/login'>
                                <Title className='menu_item_link' level={5} style={{ cursor: 'pointer', color: "white" }} >Login</Title>
                            </Link>
                        </Menu.Item>

                    }
                </Menu>
            </Sider>

        </Layout>
    )
}
