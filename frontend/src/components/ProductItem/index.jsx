import { HeartFilled, HeartOutlined, LoadingOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { Button, Col, Row, Spin } from 'antd';
import Link from 'antd/es/typography/Link';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';
import { addNewWishList, deleteFavorite, getAllFavorite } from '../../API/apiRequest';
import "../../CSS/Categories.css";
import Money from '../Money';

export default function ProductItem({ item, span }) {
    const baseURL = import.meta.env.VITE_API_PRODUCTS;
    const [loading, setLoading] = useState(true)
    const [fillHeart, setFillHeart] = useState(false)
    const navigate = useNavigate()
    const dispatch = useDispatch()

    // lấy thông tin store redux
    const user = useSelector((state) => state.auth.login?.currentUser)
    const favouriteData = useSelector((state) => state.favourite.favouriteList?.allFavourite)
    // console.log(favouriteData);

    const handleClick = () => {
        navigate(`/detail/${item._id}`)
    }

    const accessToken = user?.accessToken
    const userId = {
        userId: user?.user._id
    }

    const getDataFavourite = () => {
        getAllFavorite(userId, dispatch)
    }



    const antIcon = (
        <LoadingOutlined
            style={{
                fontSize: 24,
            }}
            spin
        />
    )
    const handleAddtoWish = async (item) => {
        if (user) {
            const newWishItem = {
                userId: user?.user._id,
                productId: item._id,
                product: item.title,
                img: item.img,
                description: item.description
            }

            addNewWishList(newWishItem, dispatch, navigate)
            setFillHeart(true)
            const updatedWishList = item._id;

            // Gửi yêu cầu cập nhật danh sách yêu thích của người dùng
            await axios.put(`${baseURL}user/update-wishList/${user?.user._id}`, {
                productId: updatedWishList
            });

            await getDataFavourite()
        } else {
            toast.warning("Đăng nhập để thêm vào danh sách yêu thích !")
        }

    }

    const handleRemoveWish = async (item) => {
        await deleteFavorite(user?.user._id, item._id, dispatch);
        setFillHeart(false);

        // Gửi yêu cầu cập nhật danh sách yêu thích của người dùng
        await axios.put(`${baseURL}user/update-wishList/${user?.user._id}`, {
            productId: item._id
        });
        await getDataFavourite();

    }

    useEffect(() => {
        // check xem có phải mảng hay không ?
        if (Array.isArray(favouriteData) && favouriteData.some(favourite => favourite.productId === item._id)) {
            setFillHeart(true);
        }
    }, [favouriteData, item._id]);



    useEffect(() => {
        const timer = setTimeout(() => {
            setLoading(false)
        }, 1300)

        return () => clearTimeout(timer);
    }, [])

    return (
        <Col key={item.id} span={span} style={{ marginBottom: 30 }} className='img_product' >
            <Spin indicator={antIcon} style={{ color: '#e83e8c' }} spinning={loading}>
                <Col className='slide_img'>
                    <Row style={{ maxWidth: 190, justifyContent: 'space-between' }}>
                        <Col>
                            <span className='text_new'>NEW</span>
                        </Col>
                        {span === 8 ? "" : <Col>
                            <span className='text_new_hot'>HOT</span>
                        </Col>}

                    </Row>
                    <img src={item.img} style={{ width: "255px" }} />
                    <div className='overlayss' onClick={handleClick}></div>
                    <Button className='add_to_carts'> <ShoppingCartOutlined />Add to cart</Button>
                    {fillHeart ? (
                        <Button className='favorite_buttons' onClick={() => handleRemoveWish(item)}>
                            <HeartFilled style={{ color: '#e83e8c' }} />
                        </Button>
                    ) : (
                        <Button className='favorite_buttons' onClick={() => handleAddtoWish(item)}>
                            <HeartOutlined />
                        </Button>
                    )}


                </Col>
            </Spin>
            <Row style={{ justifyContent: 'space-between', maxWidth: 260, marginTop: 10 }}>
                <Link>
                    <p style={{ fontSize: 13, color: "black", }}>{item.title}</p>
                </Link>
                <Col>
                    <h3><Money value={item.price} /></h3>
                </Col>
            </Row>
        </Col>

    )
}
