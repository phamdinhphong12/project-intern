const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    id: {
        type: Number,
        required: false
    },
    code: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    img: String,
    price: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    status: {
        type: String,
        required: true
    },
    orderCount: {
        type: Number,
        default: 0
    },
    quantityItem: {
        type: Number,
        required: false
    },
    wishList: {
        type: String,
        required: false
    }
}, { timestamps: true })

module.exports = mongoose.model("productModels", productSchema)