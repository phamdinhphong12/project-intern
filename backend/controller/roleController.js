const roleModels = require("../models/RoleModels")

const RoleController = {
    getAllRole: async (req, res) => {
        try {
            const Roles = await roleModels.find();
            res.status(200).json(Roles)
        } catch (e) {
            res.status(500).json({ err: e })
        }
    },

    // Add 
    addNewRole: async (req, res) => {
        try {
            const newRoles = {
                roleName: req.body.roleName,
                description: req.body.description,
                status: req.body.status
            }
            const Roles = new roleModels(newRoles)
            await Roles.save()
            res.status(200).json(Roles);

        } catch (e) {
            console.log(e);
            res.status(500).json({ Err: e })
        }
    },

    // Delete
    deleteRole: async (req, res) => {
        try {
            const RoleId = req.params.id
            const itemDelete = await roleModels.findByIdAndRemove(RoleId);
            res.status(200).json(itemDelete);

        } catch (e) {
            res.status(500).json({ Err: e })
        }
    },

    // update
    updateRoles: async (req, res) => {
        try {
            const roleId = req.params.id;
            const updateRole = {
                roleName: req.body.roleName,
                description: req.body.description,
                status: req.body.status
            }
            const query = { _id: roleId };
            const options = { new: true };
            const result = await roleModels.findOneAndUpdate(query, updateRole, options);
            res.status(200).json(result);
        } catch (e) {
            res.status(500).json({ Err: e })
        }
    },

    // Tìm kiếm
    searchRole: async (req, res) => {
        try {
            const roleName = req.query.roleName;
            const status = req.query.status;
            let query = {};
            if (roleName && status) {
                query = { roleName: roleName, status: status };
            } else if (roleName) {
                query = { roleName: roleName };
            } else if (status) {
                query = { status: status };
            }
            const Role = await roleModels.find(query);
            res.status(200).json(Role);
        } catch (e) {
            res.status(500).json({ err: e });
        }
    }

}

module.exports = RoleController