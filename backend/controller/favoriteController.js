const favoriteModels = require("../models/FavoriteModel");

const favoriteController = {
    getAllFavorites: async (req, res) => {
        try {
            const userId = req.query.userId;
            const favorites = await favoriteModels.find({ userId: userId });
            res.status(200).json(favorites);
        } catch (e) {
            res.status(500).json({ Err: e });
        }
    },

    addToFavorites: async (req, res) => {
        const userId = req.body.userId;
        const productId = req.body.productId;
        const product = req.body.product;
        const img = req.body.img;
        const description = req.body.description;

        const newFavoriteItem = new favoriteModels({
            userId,
            productId,
            product,
            img,
            description
        });

        try {
            await newFavoriteItem.save();
            res.status(200).json(newFavoriteItem);
        } catch (error) {
            console.error('Error saving to database:', error);
            res.status(500).json({ Err: error });
        }
    },

    removeFromFavorites: async (req, res) => {
        const itemId = req.body.itemId;
        const userId = req.body.userId;

        try {
            await favoriteModels.deleteOne({ productId: itemId, userId: userId });
            res.status(200).json({ message: 'Sản phẩm đã được xóa khỏi danh sách yêu thích !' });
        } catch (error) {
            console.error('Lỗi xóa sản phẩm yêu thích rồi:', error);
            res.status(500).json({ Err: error });
        }
    }
};

module.exports = favoriteController;
