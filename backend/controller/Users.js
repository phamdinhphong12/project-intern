const UsersModels = require("../models/UsersModel")
const bcrypt = require("bcrypt");

const UserController = {
    getAllUser: async (req, res) => {
        try {
            const User = await UsersModels.find();
            res.status(200).json(User)
        } catch (e) {
            res.status(500).json({ err: e })
        }
    },

    // add new
    createNewUser: async (req, res) => {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt);
        const hashConfirmPass = await bcrypt.hash(req.body.confirm, salt)

        try {
            const newUser = {
                username: req.body.username,
                avatar: req.body.avatar,
                email: req.body.email,
                password: hashedPassword,
                confirm: hashConfirmPass,
                phone: req.body.phone,
                birthday: req.body.birthday,
                sex: req.body.sex,
                address: req.body.address,
                Role: req.body.Role,
                status: req.body.status,
            }
            const Users = new UsersModels(newUser)
            await Users.save()
            res.status(200).json(Users);
            // console.log(Drinks)

        } catch (e) {
            console.log(e)
            res.status(500).json({ ERR: e })
        }
    },

    // delete
    deleteUser: async (req, res) => {
        try {
            const UserId = req.params.id
            const itemDelete = await UsersModels.findByIdAndRemove(UserId);
            res.status(200).json(itemDelete);

        } catch (e) {
            res.status(500).json({ Err: e })
        }
    },

    // update 
    updateUsers: async (req, res) => {
        // const passwordCompare = await bcrypt.compare(req.body.password);
        // const confirmCompare = await bcrypt.compare(req.body.confirm);
        try {
            const UserId = req.params.id;
            const updateUser = {
                username: req.body.username,
                avatar: req.body.avatar,
                email: req.body.email,
                password: req.body.password,
                confirm: req.body.confirm,
                phone: req.body.phone,
                birthday: req.body.birthday,
                sex: req.body.sex,
                address: req.body.address,
                Role: req.body.Role,
                status: req.body.status,

            }
            const query = { _id: UserId };
            const options = { new: true };
            const result = await UsersModels.findOneAndUpdate(query, updateUser, options);
            res.status(200).json(result);
        } catch (e) {
            res.status(500).json({ Err: e })
        }
    },

    // update wishList user
    updateWishList: async (req, res) => {
        try {
            const userId = req.params.id;
            const productId = req.body.productId;

            const user = await UsersModels.findById(userId);

            // Kiểm tra xem người dùng có tồn tại hay không
            if (!user) {
                return res.status(404).json({ message: 'User not found' });
            }

            // Kiểm tra xem sản phẩm đã tồn tại trong danh sách yêu thích hay chưa
            const index = user.wishList.indexOf(productId);

            if (index === -1) {
                // Nếu sản phẩm chưa tồn tại trong danh sách yêu thích, thêm id vào mảng
                user.wishList.push(productId);
            } else {
                // Nếu sản phẩm đã tồn tại trong danh sách yêu thích, xóa id khỏi mảng
                user.wishList.splice(index, 1);
            }


            await user.save();

            res.status(200).json(user);
        } catch (e) {
            res.status(500).json({ error: e });
        }
    },

    // Change password
    changePassword: async (req, res) => {
        try {
            const userId = req.params.id;
            const { newPassword } = req.body;

            // Tìm người dùng trong cơ sở dữ liệu
            const user = await UsersModels.findById(userId);

            // Kiểm tra xem người dùng có tồn tại hay không
            if (!user) {
                return res.status(404).json({ message: 'User not found' });
            }

            // Mã hóa mật khẩu mới
            const salt = await bcrypt.genSalt(10);
            const hashedPassword = await bcrypt.hash(newPassword, salt);

            // Cập nhật mật khẩu mới cho người dùng
            user.password = hashedPassword;

            await user.save();
            res.status(200).json(user);
        } catch (e) {
            res.status(500).json({ error: e });
        }
    },


    // Reset user password
    resetPassword: async (req, res) => {
        try {
            const userId = req.params.id;
            const user = await UsersModels.findById(userId);

            // Kiểm tra xem người dùng có tồn tại hay không
            if (!user) {
                return res.status(404).json({ message: 'User not found' });
            }
            const salt = await bcrypt.genSalt(10);
            // Cập nhật mật khẩu của người dùng
            user.password = await bcrypt.hash("123@123a", salt); // Thay đổi mật khẩu tại đây
            user.confirm = await bcrypt.hash("123@123a", salt);



            await user.save();
            res.status(200).json(user);
        } catch (e) {
            res.status(500).json({ error: e });
        }
    },

    // Search 
    searchUser: async (req, res) => {
        try {
            const email = req.query.email;
            const Role = req.query.Role;
            const status = req.query.status;
            let query = {};
            if (email && status && Role) {
                query = { email: new RegExp(email, "i"), status: status, Role: Role };
            } else if (status && Role) {
                query = { status: status, Role: Role };
            } else if (email) {
                query = { email: new RegExp(email, "i") };
            } else if (status) {
                query = { status: status };
            } else if (Role) {
                query = { Role: Role }
            }
            const Users = await UsersModels.find(query);
            res.status(200).json(Users);
        } catch (e) {
            res.status(500).json({ err: e });
        }
    }

}

module.exports = UserController