const cartModels = require("../models/CartModels")

let Cart = [];
const cartController = {

    getAllCart: async (req, res) => {
        // const userId = "6475ae57db5a7ce15ba62c0e"
        try {
            const userId = req.query.userId
            const cart = await cartModels.find({ userId: userId });
            res.status(200).json(cart);
        } catch (e) {
            res.status(500).json({ Err: e });
        }
    },

    // thêm sp vào giỏ hàng
    addNewItemCart: async (req, res) => {
        const userId = req.body.userId;
        const product = req.body.product;
        const img = req.body.img;
        const quantity = req.body.quantity;
        const size = req.body.size;
        const notes = req.body.notes;
        const price = req.body.price;
        const subtotal = quantity * price;
        const description = req.body.description

        // const existingProductIndex = Cart.findIndex(item => item.product === product);

        // if (existingProductIndex !== -1) {
        //     const existingProduct = Cart[existingProductIndex];
        //     existingProduct.quantity += quantity;
        //     existingProduct.price += quantity * price;
        //     existingProduct.subtotal += subtotal;
        // } else {
        //     const totalPrice = quantity * price;
        //     Cart.push({ product, img, quantity, size, price, subtotal: totalPrice, description });
        // }

        const newCartItem = new cartModels({
            userId,
            product,
            img,
            quantity,
            notes,
            size,
            price,
            subtotal,
            description
        });

        try {
            await newCartItem.save();
            res.status(200).json(newCartItem);
        } catch (error) {
            console.error('Error saving to database:', error);
            res.status(500).json({ Err: error });
        }
    },


    // Tăng số lượng
    increaseQuantity: async (req, res) => {
        const userId = req.body.userId;
        const itemId = req.body.itemId;
        const quantity = req.body.quantity;

        try {
            const existingProduct = await cartModels.findOne({ userId: userId, _id: itemId });

            if (existingProduct) {
                existingProduct.quantity += quantity;
                existingProduct.subtotal += quantity * existingProduct.price;
                await existingProduct.save();
                res.status(200).json({ itemId: existingProduct._id }); // Trả về ID sản phẩm
            } else {
                res.status(404).json({ message: 'Product not found in cart' });
            }
        } catch (error) {
            console.error('Error updating quantity:', error);
            res.status(500).json({ Err: error });
        }
    },

    // Giảm số lượng
    decreaseQuantity: async (req, res) => {
        const userId = req.body.userId;
        const itemId = req.body.itemId;
        const quantity = req.body.quantity;

        let existingProduct = await cartModels.findOne({ userId: userId, _id: itemId });
        // console.log(existingProduct);
        if (existingProduct) {
            if (existingProduct.quantity < 2) {
                await cartModels.deleteOne({ _id: itemId, userId: userId });
                res.status(200).json({ message: 'Sản phẩm đã xóa khỏi giỏ hàng' });
            } else {
                try {
                    existingProduct.quantity -= quantity;
                    existingProduct.subtotal -= quantity * existingProduct.price;

                    await existingProduct.save();
                    res.status(200).json(existingProduct);
                } catch (error) {
                    console.error('Error updating quantity:', error);
                    res.status(500).json({ Err: error });
                }
            }
        } else {
            res.status(404).json({ message: 'Sản phẩm không tìm thấy trong Cart' });
        }
    },

    //
    // Xóa một sản phẩm khỏi giỏ hàng
    deleteItemCart: async (req, res) => {
        const itemId = req.body.itemId;
        const userId = req.body.userId;

        try {
            await cartModels.deleteOne({ _id: itemId, userId: userId });
            res.status(200).json({ message: 'Sản phẩm đã được xóa !' });
        } catch (error) {
            console.error('Error deleting item from cart:', error);
            res.status(500).json({ Err: error });
        }
    }

}

module.exports = cartController