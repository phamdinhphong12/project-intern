const productModels = require("../models/ProductModels")

const ProductsController = {
    getAllProduct: async (req, res) => {
        try {
            const page = parseInt(req.query.page) || 1;
            const limit = parseInt(req.query.limit) || 9;
            const skip = (page - 1) * limit;

            const totalProducts = await productModels.countDocuments();
            const totalPages = Math.ceil(totalProducts / limit);

            let products;

            if (req.query.page) {
                products = await productModels.find().skip(skip).limit(limit);
            } else {
                products = await productModels.find();
            }

            res.status(200).json({
                page,
                limit,
                totalPages,
                totalProducts,
                products,
            });
        } catch (e) {
            res.status(500).json({ err: e });
        }
    },


    // Add products
    addNewProduct: async (req, res) => {
        try {
            const newProducts = {
                code: req.body.code,
                title: req.body.title,
                img: req.body.img,
                price: req.body.price,
                description: req.body.description,
                status: req.body.status,
                quantityItem: req.body.quantityItem,
                quantityItem: 500,
                wishList: "false"
            }
            const Products = new productModels(newProducts)
            await Products.save()
            res.status(200).json(Products);

        } catch (e) {
            console.log(e);
            res.status(500).json({ Err: e })
        }
    },

    // Delete
    deleteProduct: async (req, res) => {
        try {
            const productId = req.params.id
            const itemDelete = await productModels.findByIdAndRemove(productId);
            res.status(200).json(itemDelete);

        } catch (e) {
            res.status(500).json({ Err: e })
        }
    },

    // update
    updateProducts: async (req, res) => {
        try {
            const productId = req.params.id;
            const updateProduct = {
                code: req.body.code,
                title: req.body.title,
                img: req.body.img,
                price: req.body.price,
                description: req.body.description,
                status: req.body.status
            }
            const query = { _id: productId };
            const options = { new: true };
            const result = await productModels.findOneAndUpdate(query, updateProduct, options);
            res.status(200).json(result);
        } catch (e) {
            res.status(500).json({ Err: e })
        }
    },

    // update wishList 
    updateWishList: async (req, res) => {
        try {
            const productId = req.params.id;
            const updateProduct = {
                wishList: req.body.wishList
            };

            const result = await productModels.findByIdAndUpdate(productId, updateProduct, { new: true });

            res.status(200).json(result);
        } catch (e) {
            res.status(500).json({ Err: e });
        }
    },


    // Tìm kiếm
    searchProducts: async (req, res) => {
        try {
            const title = req.query.title;
            const code = req.query.code;
            const status = req.query.status;
            let query = {};
            if (title && status && code) {
                query = { title: new RegExp(title, "i"), code: new RegExp(code, "i"), status: status };
            } else if (title) {
                query = { title: new RegExp(title, "i") };
            } else if (code) {
                query = { code: new RegExp(code, "i") };
            } else if (status) {
                query = { status: status };
            }
            const Product = await productModels.find(query);
            res.status(200).json(Product);
        } catch (e) {
            res.status(500).json({ err: e });
        }
    },

    // Tăng số lượng đặt hàng cho sản phẩm
    increaseOrderCount: async (req, res) => {
        try {
            const productId = req.params.id;
            const quantityItemOrder = req.body.quantityItemOrder;
            const product = await productModels.findById(productId);
            product.orderCount += quantityItemOrder; // Tăng số lượng đặt hàng lên = số lượng đã mua
            await product.save();
            res.status(200).json(product);
        } catch (e) {
            res.status(500).json({ err: e });
        }
    }


}

module.exports = ProductsController