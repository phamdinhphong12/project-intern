const express = require("express")
const cors = require("cors");
const mongoose = require("mongoose")
const bodyParser = require('body-parser')
const cookieParser = require("cookie-parser")
const dotenv = require('dotenv')
const multer = require('multer');
const Products = require("./router/Products")
const Users = require("./router/Users")
const Auth = require("./router/Auth")
const Cart = require("./router/Cart")
const Order = require("./router/Orders")
const Role = require("./router/Role")
const Favorite = require("./router/Favorite")
const cloudinary = require('cloudinary').v2;
const fs = require('fs');


dotenv.config()
const app = express()

app.use(cookieParser())
app.use(express.static('public'));
app.use(bodyParser.json())
app.use(cors())

const PORT = process.env.PORT || 5000;

// ROUTER
app.use("/product", Products)

app.use("/user", Users)

app.use("/auth", Auth)

app.use("/cart", Cart)

app.use("/order", Order)

app.use("/role", Role)

app.use("/favorite", Favorite)


// config push img into cloudinary
cloudinary.config({
    cloud_name: 'dqdmfzohc',
    api_key: '629943818415665',
    api_secret: 'Os7LrEOkqk444qc2Q-jRXUABA08'
});

const storage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname) // tên tệp tin của ảnh sẽ được lưu trữ trên server
    }
})
const upload = multer({ storage: storage })

app.post('/upload', upload.single('file'), function (req, res, next) {
    const file = req.file;
    if (!file) {
        const error = new Error('Please upload a file');
        error.httpStatusCode = 400;
        return next(error);
    }

    // Upload ảnh lên Cloudinary
    cloudinary.uploader.upload(file.path, { folder: "Project-Intern-solashi" }, function (error, result) {
        if (error) {
            console.log('Error uploading image to Cloudinary:', error);
            return next(error);
        }

        // Xóa file tạm trên server
        fs.unlinkSync(file.path);

        // Trả về URL của ảnh đã upload trên Cloudinary
        res.send({ imageURL: result.secure_url });
    });
});



// CONNECT DATABASE
mongoose.connect(process.env.MONGODB_URL)
    .then(() => {
        console.log('CONNECT TO MONGO DB');
    })
    .catch((error) => {
        console.error('Error connecting to database', error);
    });

app.listen(PORT, () => {
    console.log("Server is running !")
})