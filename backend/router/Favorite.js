const router = require("express").Router();
const FavoriteController = require("../controller/favoriteController");

router.get("/getAllFavorite", FavoriteController.getAllFavorites);

router.post("/addToFavorite", FavoriteController.addToFavorites);

router.delete("/deleteFavorite", FavoriteController.removeFromFavorites)



module.exports = router