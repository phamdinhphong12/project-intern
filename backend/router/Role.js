const router = require("express").Router();
const RoleController = require("../controller/roleController")
const middlewareController = require("../middleware/middlewareController")

router.get("/getAllRole", middlewareController.verifyManager, RoleController.getAllRole)

router.post("/addNewRole", RoleController.addNewRole);

router.delete("/:id", RoleController.deleteRole);

router.put("/update/:id", RoleController.updateRoles);

router.get("/search", RoleController.searchRole)


module.exports = router