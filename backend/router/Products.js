const router = require("express").Router();
const ProductsController = require("../controller/Product");
const middlewareController = require("../middleware/middlewareController")

router.get("/getAll", ProductsController.getAllProduct);

router.post("/addNew", ProductsController.addNewProduct);

router.post("/increaseOrderCount/:id", ProductsController.increaseOrderCount)

router.delete("/:id", ProductsController.deleteProduct);

router.put("/update/:id", ProductsController.updateProducts);

router.put("/update-wishList/:id", ProductsController.updateWishList)

router.get('/search', ProductsController.searchProducts);


module.exports = router