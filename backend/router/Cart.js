const router = require("express").Router();
const cartController = require("../controller/CartController");

router.get("/getAllCart", cartController.getAllCart)

router.post("/addItemCart", cartController.addNewItemCart);

router.put("/product", cartController.increaseQuantity)

router.put("/decrease", cartController.decreaseQuantity)

router.delete("/delete", cartController.deleteItemCart)


module.exports = router