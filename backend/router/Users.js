const router = require("express").Router();
const UserController = require("../controller/Users")
const middlewareController = require("../middleware/middlewareController")

router.get("/getAllUser", UserController.getAllUser);

router.post("/addNewUser", UserController.createNewUser);

router.delete("/:id", UserController.deleteUser);

router.put("/update/:id", UserController.updateUsers);

router.post("/reset/:id", UserController.resetPassword);

router.get("/search", UserController.searchUser)

router.put("/changePassword/:id", UserController.changePassword)

router.put("/update-wishList/:id", UserController.updateWishList)



module.exports = router